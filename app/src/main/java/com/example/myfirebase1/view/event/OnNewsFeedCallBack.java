package com.example.myfirebase1.view.event;

import android.os.Bundle;

import com.example.myfirebase1.model.Image;
import com.example.myfirebase1.model.UserInfo;

import java.util.ArrayList;

public interface OnNewsFeedCallBack extends OnCallBackToView{
    void toLikeSuccess(int position,int count, boolean like);

    void toLikeNotSuccess();

    void toCommentNotSuccess();

    void toCommentSuccess(Bundle bundle);

    void toNotSuccessDataChange();

    void toSuccessDataChange(ArrayList<Image> dataImage);

    void toSuccessAvatarDataChange(ArrayList<Image> dataImage);

    void toNotSuccessAvatarDataChange();

    void toChangeAvatar(UserInfo info, Image d);

    void toDisLikeSuccess(int position, int count, boolean like);
}
