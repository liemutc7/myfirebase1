package com.example.myfirebase1.utils;

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.text.TextPaint;
import android.widget.TextView;

public class TextUtils {
    private TextUtils(){}
    private static TextUtils instance;

    public static TextUtils getInstance(){
        if(instance == null){
            instance = new TextUtils();
        }
        return instance;
    }

    public void setTextColor(TextView textView){
        textView.setText(textView.getText().toString().toUpperCase());

        TextPaint paint = textView.getPaint();
        float width = paint.measureText(textView.getText().toString());

        Shader textShader = new LinearGradient(0, 0, width, textView.getTextSize(),
                new int[]{
                        Color.parseColor("#2B292E"),
                        Color.parseColor("#A70000"),
                        Color.parseColor("#FF4500")
                }, null, Shader.TileMode.CLAMP);
        textView.getPaint().setShader(textShader);
    }

}
