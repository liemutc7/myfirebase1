package com.example.myfirebase1.presenter;


import com.example.myfirebase1.view.event.OnCallBackToView;

public abstract class BasePresenter<T extends OnCallBackToView> {
    protected T mCallBack;

    public BasePresenter(T mCallBack) {
        this.mCallBack = mCallBack;
    }

}
