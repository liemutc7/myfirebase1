package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnChatRoomCallBack;

public class ChatRoomPresenter extends BasePresenter<OnChatRoomCallBack>{
    public ChatRoomPresenter(OnChatRoomCallBack mCallBack) {
        super(mCallBack);
    }
}
