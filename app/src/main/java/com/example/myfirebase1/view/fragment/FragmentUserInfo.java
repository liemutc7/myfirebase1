package com.example.myfirebase1.view.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragUserInfoBinding;
import com.example.myfirebase1.model.Image;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.UserInfoPresenter;
import com.example.myfirebase1.utils.CommonUtils;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.view.event.OnUserInfoCallBack;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FragmentUserInfo extends BaseFragment<UserInfoPresenter, FragUserInfoBinding>
        implements OnUserInfoCallBack, ValueEventListener, View.OnClickListener {


    public static final String TAG = FragmentUserInfo.class.getName();


    private ArrayList<UserInfo> dataUserInfo;
    private String mailInfo = "";
    private String uidInfo = "";
    private String urlAvatar = "";

    @Override
    protected UserInfoPresenter getPresenter() {
        return new UserInfoPresenter(this);
    }

    @Override
    protected void initView() {
        setTitle("Profile");
        if(getArguments()==null){
            myAvatar = PrefUtil.getString(mContext,Constances.MY_AVATAR_URL,"");
            mailInfo = mCurrentUser.getEmail();
            uidInfo = mCurrentUser.getUid();
            urlAvatar = myAvatar;
        }else {
//            Image image = (Image) getArguments().getSerializable(Constances.SERI_ITEM_IMAGE_DIFF);
            mailInfo = getArguments().getString(Constances.USER_MAIL_DIFF,"");
            uidInfo = getArguments().getString(Constances.USER_UID_DIFF,"");
            urlAvatar = getArguments().getString(Constances.USER_AVATAR_URL_DIFF,"");
        }
        dataUserInfo = new ArrayList<>();
        myRefUserInfo.addValueEventListener(this);
        binding.ivSendChatList.setOnClickListener(this);
        if(myEmail.equals(mailInfo)){
            binding.ivSendChatList.setVisibility(View.GONE);
        }
        binding.ivUserAvatar.setOnClickListener(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_user_info;
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//        showNotify("User info ...");
        mPresenter.dataUserInfo(dataUserInfo, dataSnapshot);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void toSuccessDataChange(ArrayList<UserInfo> dataUserInfo) {
        for (UserInfo i : dataUserInfo) {
            if (mailInfo.equals(i.getEmailUser())){
                Glide.with(mContext)
                        .load(i.getUrlAvatarUser())
                        .centerCrop()
                        .circleCrop()
                        .into(binding.ivUserAvatar);
                binding.tvUserEmail.setText(i.getEmailUser());
                binding.tvUserDistance.setText(CommonUtils.getInstance().formatDoubleToString(i.getTotalRunUser()));
//                binding.setUserInfo(i);
            }
        }
    }


    @Override
    public void toNotSuccessDataChange() {
//        showNotify("toNotSuccessDataChange...user Info");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_send_chat_list:
                Bundle bundle = new Bundle();
                bundle.putString(Constances.CHAT_LIST_MAIL,mailInfo);
                bundle.putString(Constances.CHAT_LIST_UID,uidInfo);
                getParent().showFragment(FragmentChatList.TAG,FragmentUserInfo.TAG,bundle);
                break;
            case R.id.iv_user_avatar:
                Bundle bundleAvatar = new Bundle();
                bundleAvatar.putString(Constances.NEWS_FEED_IMAGE,urlAvatar);
                getParent().showFragment(FragmentImageReview.TAG,FragmentUserInfo.TAG,bundleAvatar);
                break;
            default:
                break;
        }
    }
}
