package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnLocationReviewCallBack;

public class LocationReviewPresenter extends BasePresenter<OnLocationReviewCallBack>{
    public LocationReviewPresenter(OnLocationReviewCallBack mCallBack) {
        super(mCallBack);
    }
}
