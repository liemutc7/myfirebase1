package com.example.myfirebase1.view.fragment;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import android.view.View;

import com.example.myfirebase1.R;
import com.example.myfirebase1.adapter.RankDistanceAdapter;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragRankDistanceBinding;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.RankDistancePresenter;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.view.event.OnRankDistanceCallBack;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FragmentRankDistance extends BaseFragment<RankDistancePresenter, FragRankDistanceBinding>
        implements OnRankDistanceCallBack, ValueEventListener, RankDistanceAdapter.RankListener {

    public static final String TAG = FragmentRankDistance.class.getName();
    private ArrayList<UserInfo> dataInfo;
//    private ArrayList<UserInfo> CustomData;
    private RankDistanceAdapter distanceAdapter;

    @Override
    protected RankDistancePresenter getPresenter() {
        return new RankDistancePresenter(this);
    }

    @Override
    protected void initView() {
        setTitle("Total Distance");
        dataInfo = new ArrayList<>();
        myRefUserInfo.addValueEventListener(this);
        distanceAdapter = new RankDistanceAdapter(mContext);
//        distanceAdapter.setData(dataInfo);
        distanceAdapter.setListener(this);
        binding.rvRankDistance.setAdapter(distanceAdapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_rank_distance;
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        mPresenter.dataChangeRank(dataInfo, dataSnapshot);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void toSuccessRankDataChange(ArrayList<UserInfo> dataInfo) {
        distanceAdapter.setData(dataInfo);
        distanceAdapter.notifyDataSetChanged();
    }

    @Override
    public void toNotSuccessRankDataChange() {
        showNotify("toNotSuccessRankDataChange");
    }

    @Override
    public void toInfo(String email) {
        Bundle bundle = new Bundle();
        bundle.putString(Constances.USER_MAIL_DIFF,email);
        getParent().showFragment(FragmentUserInfo.TAG,FragmentRankDistance.TAG,bundle);
    }
}
