package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnChangeAvatarCallBack;

public class ChangeAvatarPresenter extends BasePresenter<OnChangeAvatarCallBack>{
    public ChangeAvatarPresenter(OnChangeAvatarCallBack mCallBack) {
        super(mCallBack);
    }
}
