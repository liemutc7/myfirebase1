package com.example.myfirebase1.view.event;

public interface OnCommentCallBack extends OnCallBackToView{
    void toLikeSuccess(int count);

    void toLikeNotSuccess();
}
