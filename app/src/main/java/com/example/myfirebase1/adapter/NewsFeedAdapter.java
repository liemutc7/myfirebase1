package com.example.myfirebase1.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.databinding.ItemImageBinding;
import com.example.myfirebase1.model.Image;
import com.example.myfirebase1.utils.CommonUtils;

import java.util.ArrayList;

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.ViewHolder> {

    private ArrayList<Image> data;
    private LayoutInflater inflater;
    private Context context;
    private ImageListener imageListener;

    public void setImageListener(ImageListener imageListener) {
        this.imageListener = imageListener;
    }

    public NewsFeedAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setData(ArrayList<Image> data) {
        this.data = data;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemImageBinding binding = ItemImageBinding.inflate(inflater);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.bindData(data.get(i), i, imageListener);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemImageBinding binding;

        public ViewHolder(@NonNull ItemImageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(final Image item, final int position, final ImageListener listener) {

            if (item.getName().equals("")) {
                binding.tvItemName.setVisibility(View.GONE);
            }
            binding.tvItemName.setText(item.getName());
            binding.tvItemDatePost.setText(item.getDate());
            binding.tvItemMail.setText(CommonUtils.getInstance().spilitEmail(item.getMail()));
            binding.tvCountLike.setText(String.valueOf(item.getCountLike()));
            binding.tvCountComment.setText(String.valueOf(item.getCountComment()));

            if (item.getUrl() != null) {
                Glide.with(context)
                        .load(item.getUrl())
                        .centerCrop()
                        .into(binding.ivItemImage);
            } else {
                binding.ivItemImage.getLayoutParams().height = 0;
            }

            if (item.getUrlAvatar() != null) {
                Glide.with(context)
                        .load(item.getUrlAvatar())
                        .centerCrop()
                        .circleCrop()
                        .into(binding.ivItemAvatar);
            }

            binding.ivIconLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.toLike(position);
                    }
                }
            });

            binding.ivIconLike.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (listener != null) {
                        listener.toDisLike(position);
                    }
                    return true;
                }
            });


            binding.ivComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.toComment(position);
                    }
                }
            });

            binding.ivItemAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.toUserInfo(item);
                    }
                }
            });

            binding.tvItemMail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.toUserInfo(item);
                    }
                }
            });


            if (item.getTotalDistance() != null && item.getMaxSpeed() != null
                    && item.getLatitude() != null && item.getLongitude() != null
                    && item.getArrLat() != null && item.getArrLong() != null) {
                binding.tvDistance.setText(item.getTotalDistance());
                binding.tvSpeed.setText(item.getMaxSpeed());
                binding.ivItemImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.toImageLocationArrayPoint(item.getLatitude(), item.getLongitude(),
                                    item.getArrLat(), item.getArrLong());
                        }
                    }
                });
            } else if (item.getTotalDistance() != null && item.getMaxSpeed() != null
                    && item.getLatitude() != null && item.getLongitude() != null) {
                binding.tvDistance.setText(item.getTotalDistance());
                binding.tvSpeed.setText(item.getMaxSpeed());
                binding.ivItemImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.toImageLocation(item.getLatitude(), item.getLongitude());
                        }
                    }
                });
            } else {
                binding.tvDistance.setVisibility(View.GONE);
                binding.tvSpeed.setVisibility(View.GONE);
                binding.ivItemImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.toImage(item);
                        }
                    }
                });
            }


        }
    }

    public interface ImageListener {
        void toLike(int position);

        void toDisLike(int position);

        void toComment(int position);

        void toImage(Image item);

        void toUserInfo(Image item);

        void toImageLocation(String latitude, String longitude);

        void toImageLocationArrayPoint(String latitude, String longitude,
                                       ArrayList<String> arrLat,
                                       ArrayList<String> arrLong);
    }

}
