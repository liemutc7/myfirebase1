package com.example.myfirebase1.view.fragment;

import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragTestRunBinding;
import com.example.myfirebase1.presenter.TestPresenter;
import com.example.myfirebase1.view.event.OnTestCallBack;

public class FragmentTest extends BaseFragment<TestPresenter, FragTestRunBinding> implements OnTestCallBack {

    public static final String TAG = FragmentTest.class.getName();

    @Override
    protected TestPresenter getPresenter() {
        return new TestPresenter(this);
    }

    @Override
    protected void initView() {
        setTitle("Test");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_test_run;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
