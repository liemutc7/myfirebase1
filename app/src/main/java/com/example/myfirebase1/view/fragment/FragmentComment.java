package com.example.myfirebase1.view.fragment;

import androidx.annotation.NonNull;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.myfirebase1.R;
import com.example.myfirebase1.adapter.CommentAdapter;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragCommentBinding;
import com.example.myfirebase1.model.Chat;
import com.example.myfirebase1.model.Image;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.CommentPresenter;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.view.event.OnCommentCallBack;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FragmentComment extends BaseFragment<CommentPresenter, FragCommentBinding> implements OnCommentCallBack, ValueEventListener, View.OnClickListener, OnSuccessListener<Void>, OnFailureListener {

    public static final String TAG = FragmentComment.class.getName();

    private ArrayList<Chat> dataChat = null;
    private CommentAdapter chatAdapter;
    private int countComment;
    private String mEmails;
    private String mIds;
    private int countLike;
    private boolean mIsLike;

    private ArrayList<Image> dataTest;


    @Override
    protected CommentPresenter getPresenter() {
        return new CommentPresenter(this);
    }

    @Override
    protected void initView() {
        if (getArguments() != null) {
            getArgs();
        }
        setTitle("Comment");
        dataChat = new ArrayList<>();
        chatAdapter = new CommentAdapter(mContext);
        binding.lvComment.setAdapter(chatAdapter);
        chatAdapter.setData(dataChat);
//        myRef = mDatabase.getReference("Images");
        myRef.child(mIds).child("comment").addValueEventListener(this);
        binding.ivSendComment.setOnClickListener(this);
        binding.ivCountLike.setOnClickListener(this);
        myAvatar = PrefUtil.getString(mContext, Constances.MY_AVATAR_URL, "");
        myRefUserInfo.addValueEventListener(this);

        binding.ivCountLike.setText("" + countLike);
        if (dataTest.size() > 0) {
            showNotify("Data test size = " + dataTest.size());
            //test111
        }
        myRef.child(mIds).child("countLike").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                hideLoading();
                Log.d("datass", " onDataChange " + dataSnapshot.getValue());
                binding.ivCountLike.setText(dataSnapshot.getValue().toString());
                hideLoading();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("datass", " onCancelled " + databaseError.getMessage());
            }
        });
    }

    private void getArgs() {
        //test bug 3
        mEmails = getArguments().getString(Constances.EMAIL_IMAGE, "");
        mIds = getArguments().getString(Constances.ID_IMAGE, "");
        countComment = getArguments().getInt(Constances.COUNT_COMMENT_IMAGE, 0);
        countLike = getArguments().getInt(Constances.COUNT_LIKE_IMAGE, 0);
        mIsLike = getArguments().getBoolean(Constances.IS_LIKE_IMAGE, false);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.frag_comment;
    }

    @Override
    public void onResume() {
        super.onResume();
//        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
//        CommonUtils.getInstance().changeStatusBar(getActivity(),R.color.ColorBlack);
    }

    @Override
    public void onStop() {
        super.onStop();
//        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
//        CommonUtils.getInstance().changeStatusBar(getActivity(),R.color.ColorSplash);

    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

        if (dataSnapshot.getRef().equals(myRefUserInfo)) {
            showLoading();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                UserInfo info = data.getValue(UserInfo.class);
                for (Chat d : dataChat) {
                    if (info.getEmailUser().equals(d.getEmail())) {
                        myRef.child(mIds).child("comment").child("" + dataChat.indexOf(d)).child("avatar").setValue(info.getUrlAvatarUser());
//                        d.setAvatar(info.getUrlAvatarUser());
                    }
                }
            }
            chatAdapter.setData(dataChat);
            chatAdapter.notifyDataSetChanged();
            hideLoading();

        } else {
            try {
                dataChat.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Chat chat = data.getValue(Chat.class);
                    dataChat.add(chat);
//            Gson gson = new Gson();
//            Log.i(TAG,"Gsons : "+gson.toJson(data.getValue()));
                }
                chatAdapter.setData(dataChat);
                chatAdapter.notifyDataSetChanged();
                hideLoading();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_send_comment:
                if (binding.edtComment.getText().toString().equals("") || binding.edtComment.getText() == null) {
                    return;
                }
                String text = binding.edtComment.getText().toString();
                Chat chat = new Chat();
                chat.setEmail(myEmail);
                chat.setText(text);
                chat.setAvatar(myAvatar);
                dataChat.add(chat);
                myRef.child(mIds).child("comment").setValue(dataChat).addOnSuccessListener(this)
                        .addOnFailureListener(this);
                binding.edtComment.setText("");
                break;
            case R.id.iv_count_like:
                mPresenter.toLike(countLike);
                break;
            default:
                break;
        }
    }

    @Override
    public void onSuccess(Void aVoid) {
//        showNotify("onSuccess comment");
        countComment++;
        myRef.child(mIds).child("countComment").setValue(countComment);
        binding.edtComment.setText("");
        binding.lvComment.scrollToPosition(dataChat.size() - 1);
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        Toast.makeText(getParent(), "onFailure", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }

    @Override
    public void toLikeSuccess(int count) {
        myRef.child(mIds).child("countLike").setValue(count);
        myRef.child(mIds).child("like").setValue(!mIsLike);
        this.countLike = count;
        this.mIsLike = !mIsLike;
    }

    @Override
    public void toLikeNotSuccess() {
        showNotify("Error like");
    }
}
