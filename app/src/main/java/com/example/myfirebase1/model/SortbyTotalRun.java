package com.example.myfirebase1.model;


import java.util.Comparator;

public class SortbyTotalRun implements Comparator<UserInfo>
{ 
    // Used for sorting in ascending order of 
    // roll number
    @Override
    public int compare(UserInfo a, UserInfo b) {
        return (int) (b.getTotalRunUser()*1000 - a.getTotalRunUser()*1000);
    }
}