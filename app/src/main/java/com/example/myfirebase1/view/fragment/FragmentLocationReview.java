package com.example.myfirebase1.view.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragLocationReviewBinding;
import com.example.myfirebase1.presenter.LocationReviewPresenter;
import com.example.myfirebase1.utils.CommonUtils;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.view.event.OnLocationReviewCallBack;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class FragmentLocationReview extends BaseFragment<LocationReviewPresenter, FragLocationReviewBinding>
        implements OnLocationReviewCallBack, OnMapReadyCallback, LocationListener {

    public static final String TAG = FragmentLocationReview.class.getName();


    private Geocoder geocoder;
    private Marker marker;
    private Marker currentLocationMarker;
    private String latitude;
    private String longitude;
    private PolylineOptions polylineOptions;
    private ArrayList<LatLng> arrayPoints = new ArrayList<>();
    ArrayList<String> arrLat = new ArrayList<>();
    ArrayList<String> arrLong = new ArrayList<>();


    private final String[] PERMISSION = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private GoogleMap mMap;
    private View mapView;

    @Override
    protected LocationReviewPresenter getPresenter() {
        return new LocationReviewPresenter(this);
    }

    @Override
    protected void initView() {
        showLoading();
        if (checkPermission()) {
            init();
        }
    }


    @SuppressLint("MissingPermission")
    private void initMap() {
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);//cho phep zoom in zoom out map len
        LocationManager manager = (LocationManager) mContext.getSystemService(mContext.LOCATION_SERVICE);
        if (getArguments() != null) {
            latitude = getArguments().getString(Constances.IMAGE_LATITUDE,"21.024392");
            longitude = getArguments().getString(Constances.IMAGE_LONGITUDE,"105.805197");
            LatLng lng = new LatLng(Float.parseFloat(latitude),Float.parseFloat(longitude));
            CameraPosition position = new CameraPosition(
                    lng, 15, 0, 0);
            //v:zoom
            //v1:tilt:Góc, tính theo độ, của góc camera từ nadir (đối diện trực tiếp với Trái đất)
            //       //. Xem độ nghiêng (float) để biết chi tiết về các hạn chế về phạm vi giá trị
            //       //. phạm vi từ 0 đến 90 độ.
            //v2:bearing://Hướng mà máy ảnh hướng vào, theo độ theo chiều kim đồng hồ từ phía bắc.
            //        // Giá trị này sẽ được chuẩn hóa trong phạm vi bao gồm 0 độ và độc quyền 360 độ
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(lng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            mMap.addMarker(markerOptions);

            if(getArguments().getStringArrayList(Constances.IMAGE_ARRAY_POINT_LAT)!=null){
                arrLat = getArguments().getStringArrayList(Constances.IMAGE_ARRAY_POINT_LAT);
                arrLong = getArguments().getStringArrayList(Constances.IMAGE_ARRAY_POINT_LONG);
                ArrayList<LatLng> arrLatLng = new ArrayList<>();
                for (int i = 0; i < arrLat.size(); i++) {
                    LatLng latLng = new LatLng(Double.parseDouble(arrLat.get(i)), Double.parseDouble(arrLong.get(i)));
                    arrLatLng.add(latLng);
                    if (currentLocationMarker != null) {
                        currentLocationMarker.remove();
                    }
//                    LatLng latLng = new LatLng(a.getLatitude(), a.getLongitude());
//                    LatLngEntity latLngEntity = new LatLngEntity(location.getLatitude(),location.getLongitude());
//                    MarkerOptions marker = new MarkerOptions();
//                    marker.position(latLng);
//                    currentLocationMarker = mMap.addMarker(marker);
                    polylineOptions = new PolylineOptions();
                    polylineOptions.color(Color.BLUE);
                    polylineOptions.width(5);
                    arrayPoints.add(latLng);
                    polylineOptions.addAll(arrayPoints);
                    mMap.addPolyline(polylineOptions);
                }

            }

        }
    }


    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String s : PERMISSION) {
                if (ActivityCompat.checkSelfPermission(mContext, s) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(PERMISSION, 0);
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (checkPermission()) {
            init();
        } else {
            getParent().showFragment(FragmentNewsFeed.TAG, null, null);
        }
    }

    private void init() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_review);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        CommonUtils.getInstance().changeStatusBar(getActivity(),R.color.ColorBlackTransD);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
        CommonUtils.getInstance().changeStatusBar(getActivity(),R.color.ColorSplash);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.frag_location_review;
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        geocoder = new Geocoder(mContext);
        initMap();

        //custom my location button
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 180, 180, 0);
        }


        hideLoading();

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
