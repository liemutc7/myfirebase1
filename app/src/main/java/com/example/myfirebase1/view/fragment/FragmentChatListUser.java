package com.example.myfirebase1.view.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;

import android.view.View;

import com.example.myfirebase1.R;
import com.example.myfirebase1.adapter.ChatListAdapter;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragChatListUserBinding;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.ChatListUserPresenter;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.view.event.OnChatListUserCallBack;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FragmentChatListUser extends BaseFragment<ChatListUserPresenter, FragChatListUserBinding>
        implements OnChatListUserCallBack, ValueEventListener, ChatListAdapter.ItemChatListClickListener, View.OnClickListener {

    public static final String TAG = FragmentChatListUser.class.getName();

    private ChatListAdapter adapter;
    private ArrayList<UserInfo> dataUser;


    @Override
    protected ChatListUserPresenter getPresenter() {
        return new ChatListUserPresenter(this);
    }

    @Override
    protected void initView() {
        setTitle("Chat List");
        adapter = new ChatListAdapter(mContext);
        dataUser = new ArrayList<>();
        binding.rvChatListUser.setAdapter(adapter);
        adapter.setListener(this);
        myRefUserInfo.addValueEventListener(this);
        binding.rlChatRoom.setOnClickListener(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_chat_list_user;
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        mPresenter.toDataChange(dataUser, dataSnapshot);

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void onItemClick(UserInfo chat) {
        Bundle bundle = new Bundle();
        if (chat.getEmailUser() == null || chat.getUidUser() == null) {
            return;
        }
        bundle.putString(Constances.CHAT_LIST_MAIL,chat.getEmailUser());
        bundle.putString(Constances.CHAT_LIST_UID,chat.getUidUser());
        bundle.putString(Constances.CHAT_LIST_AVATAR_URL,chat.getUrlAvatarUser());
        getParent().showFragment(FragmentChatList.TAG,FragmentChatListUser.TAG,bundle);
    }

    @Override
    public void onLongItemClick(UserInfo chat) {

    }

    @Override
    public void toSuccessDataChange(ArrayList<UserInfo> dataUser) {
        adapter.setData(dataUser);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void toNotSuccessDataChange() {
        showNotify("toNotSuccessDataChange...load list user");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_chat_room:
                toChatRoom();
                break;
            default:
                break;
        }
    }

    private void toChatRoom() {
        getParent().showFragment(FragmentChatRoom.TAG, FragmentChatListUser.TAG, null);
    }
}
