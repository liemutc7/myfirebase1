package com.example.myfirebase1.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.R;
import com.example.myfirebase1.databinding.ItemChatBinding;

import com.example.myfirebase1.databinding.ItemChatRightBinding;
import com.example.myfirebase1.model.Chat;
import com.example.myfirebase1.utils.CommonUtils;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private ArrayList<Chat> data;
    private LayoutInflater inflater;
    private ItemClickListener listener;
    private String myEmail;

    public ChatAdapter(Context context,String myEmail) {
        inflater = LayoutInflater.from(context);
        this.myEmail = myEmail;
//        this.data = data;
    }

    public void setData(ArrayList<Chat> data) {
        this.data = data;
    }

    public void setListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
//        ItemChatBinding binding = ItemChatBinding.inflate(inflater);
//        return new CommentAdapter.MyViewHolder(binding);
        ViewDataBinding binding;

        MyViewHolder holder = null;
        switch (viewType) {
            case R.layout.item_chat:
                binding = ItemChatBinding.inflate(inflater);
                holder = new ChatAdapter.FirstViewHolder((ItemChatBinding) binding);
                break;
            case R.layout.item_chat_right:
                binding = ItemChatRightBinding.inflate(inflater);
                holder = new ChatAdapter.SecondViewHolder((ItemChatRightBinding) binding);
                break;
        }
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).getEmail().equals(myEmail)) {
            return R.layout.item_chat_right;
        } else {
            return R.layout.item_chat;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int i) {
        viewHolder.bind(data.get(i));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener != null) {
                    listener.onItemClick(data.get(i));
                }
            }
        });
        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (listener != null) {
                    listener.onLongItemClick(data.get(i));
                }
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public static abstract class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(View itemView) {
            super(itemView);
            // perform action specific to all viewholders, e.g.
            // ButterKnife.bind(this, itemView);
        }

        abstract void bind(Chat item);
    }

    public class FirstViewHolder extends MyViewHolder {
        private ItemChatBinding binding;

        public FirstViewHolder(@NonNull ItemChatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        void bind(Chat item) {
            binding.tvEmailChat.setText(CommonUtils.getInstance().spilitEmail(item.getEmail()));
            binding.tvTextChat.setText(item.getText());
            Glide.with(itemView.getContext())
                    .load(item.getAvatar())
                    .circleCrop()
                    .into(binding.ivAvatar);
        }
    }

    public class SecondViewHolder extends MyViewHolder {
        private ItemChatRightBinding binding;

        public SecondViewHolder(@NonNull ItemChatRightBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        void bind(Chat item) {
            binding.tvEmailChat.setText(CommonUtils.getInstance().spilitEmail(item.getEmail()));
            binding.tvTextChat.setText(item.getText());
            Glide.with(itemView.getContext())
                    .load(item.getAvatar())
                    .circleCrop()
                    .into(binding.ivAvatar);
        }
    }

    public interface ItemClickListener {
        void onItemClick(Chat chat);

        void onLongItemClick(Chat chat);
    }
}

