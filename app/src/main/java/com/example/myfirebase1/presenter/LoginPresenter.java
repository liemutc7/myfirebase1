package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnLoginCallBack;

public class LoginPresenter extends BasePresenter<OnLoginCallBack>{
    public LoginPresenter(OnLoginCallBack mCallBack) {
        super(mCallBack);
    }
}
