package com.example.myfirebase1.view.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.example.myfirebase1.R;
import com.example.myfirebase1.adapter.ChatAdapter;
import com.example.myfirebase1.adapter.CommentAdapter;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragChatListBinding;
import com.example.myfirebase1.model.Chat;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.ChatListPresenter;
import com.example.myfirebase1.utils.CommonUtils;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.view.event.OnChatListCallBack;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FragmentChatList extends BaseFragment<ChatListPresenter, FragChatListBinding> implements OnChatListCallBack, ValueEventListener, View.OnClickListener, OnSuccessListener<Void>, OnFailureListener {

    public static final String TAG = FragmentChatList.class.getName();

    private ArrayList<Chat> dataChat = null;
    private ChatAdapter chatAdapter;
    private String mChatMail;
    private String mChatUid;
    private String mChatAvatar;
    private String mMyUid;
    private DatabaseReference myRefChatList = mDatabase.getReference();
    private DatabaseReference myRefChatListDiff = mDatabase.getReference();


    @Override
    protected ChatListPresenter getPresenter() {
        return new ChatListPresenter(this);
    }

    @Override
    protected void initView() {
        if (getArguments() != null) {
            mChatMail = getArguments().getString(Constances.CHAT_LIST_MAIL, "");
            mChatUid = getArguments().getString(Constances.CHAT_LIST_UID, "");
            mChatAvatar = getArguments().getString(Constances.CHAT_LIST_AVATAR_URL, "");
        }
        mMyUid = mCurrentUser.getUid();
        myEmail = mCurrentUser.getEmail();
        dataChat = new ArrayList<>();
        chatAdapter = new ChatAdapter(mContext,myEmail);
        chatAdapter.setData(dataChat);
        binding.lvChatList.setAdapter(chatAdapter);
        myRefChatList = mDatabase.getReference("ChatLists").child("" + mMyUid).child("" + mChatUid);
        myRefChatListDiff = mDatabase.getReference("ChatLists").child("" + mChatUid).child("" + mMyUid);
        myRefChatList.addValueEventListener(this);

//        myRefChatList.child(""+mChatUid).addValueEventListener(this);
        binding.ivSendChatList.setOnClickListener(this);
        myAvatar = PrefUtil.getString(mContext, Constances.MY_AVATAR_URL, "");

        binding.ivBack.setOnClickListener(this);
        binding.trInfo.setOnClickListener(this);
        myRefUserInfo.addValueEventListener(this);

        if (!mChatAvatar.isEmpty()) {
            Glide.with(mContext)
                    .load(mChatAvatar)
                    .circleCrop()
                    .into(binding.ivAvatarHead);
        }
        binding.tvMailHead.setText(CommonUtils.getInstance().spilitEmail(mChatMail));


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_chat_list;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

        if (dataSnapshot.getRef().equals(myRefUserInfo)) {
            showLoading();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                final UserInfo info = data.getValue(UserInfo.class);
                for (final Chat d : dataChat) {
                    if (info.getEmailUser().equals(d.getEmail())) {
                        myRefChatList.child(d.getId() + "").child("avatar").setValue(info.getUrlAvatarUser());
                        myRefChatListDiff.child(d.getId() + "").child("avatar").setValue(info.getUrlAvatarUser());
//                        d.setAvatar(info.getUrlAvatarUser());
                    }
                }
            }
            chatAdapter.setData(dataChat);
            chatAdapter.notifyDataSetChanged();
            hideLoading();
        } else {

            try {
                showLoading();
                dataChat.clear();
                for (DataSnapshot s : dataSnapshot.getChildren()) {
                    Chat chat = s.getValue(Chat.class);
                    dataChat.add(chat);
                    if (chat.getEmail().equals(mChatMail)) {
                        mChatAvatar = chat.getAvatar();
                    }
                }
                chatAdapter.setData(dataChat);
                chatAdapter.notifyDataSetChanged();
                binding.tvMailHead.setText("" + mChatMail);
                Glide.with(mContext)
                        .load(mChatAvatar)
                        .circleCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .signature(new ObjectKey(String.valueOf(System.currentTimeMillis())))
                        .into(binding.ivAvatarHead);
                hideLoading();
            } catch (Exception ex) {
                ex.printStackTrace();
                hideLoading();
            }
        }


    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_send_chat_list:
                toSendChatList();
                break;
            case R.id.iv_back:
                getActivity().onBackPressed();
                break;
            case R.id.tr_info:
                Bundle bundle = new Bundle();
                bundle.putString(Constances.USER_MAIL_DIFF, mChatMail);
                bundle.putString(Constances.USER_UID_DIFF, mChatUid);
                getParent().showFragment(FragmentUserInfo.TAG, FragmentChatList.TAG, bundle);
                break;
            default:
                break;
        }
    }

    private void toSendChatList() {
        if (binding.edtChatList.getText().toString().isEmpty()) {
            return;
        }
        if (mCurrentUser.getEmail().isEmpty()) {
            return;
        }

        String textChat = binding.edtChatList.getText().toString();
        String emailChat = mCurrentUser.getEmail();
        Chat chat = new Chat();
        chat.setEmail(emailChat);
        chat.setText(textChat);
        chat.setAvatar(myAvatar);
        dataChat.add(chat);


        myRefChatList.child("" + chat.getId()).setValue(chat).addOnSuccessListener(this)
                .addOnFailureListener(this);
        myRefChatListDiff.child("" + chat.getId()).setValue(chat).addOnSuccessListener(this)
                .addOnFailureListener(this);

        binding.edtChatList.setText("");

    }

    @Override
    public void onSuccess(Void aVoid) {
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        Toast.makeText(mContext, "Fail img_send", Toast.LENGTH_SHORT).show();
    }
}
