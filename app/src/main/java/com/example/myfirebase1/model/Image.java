package com.example.myfirebase1.model;

import java.util.ArrayList;

public class Image{
    private long id = System.currentTimeMillis();
    private String name;
    private String url;
    private String date;
    private String mail;
    private String uid;
    private int countLike = 0;
    private int countComment = 0;
    private boolean like = false;
    private ArrayList<Chat> comment = null;
    private String urlAvatar;
    private String totalDistance;
    private String maxSpeed;
    private String latitude;
    private String longitude;
    private ArrayList<String> arrLat = null;
    private ArrayList<String> arrLong = null;

    public Image() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getCountLike() {
        return countLike;
    }

    public void setCountLike(int countLike) {
        this.countLike = countLike;
    }

    public int getCountComment() {
        return countComment;
    }

    public void setCountComment(int countComment) {
        this.countComment = countComment;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public ArrayList<Chat> getComment() {
        return comment;
    }

    public void setComment(ArrayList<Chat> comment) {
        this.comment = comment;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }


    public String getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(String maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public ArrayList<String> getArrLat() {
        return arrLat;
    }

    public void setArrLat(ArrayList<String> arrLat) {
        this.arrLat = arrLat;
    }

    public ArrayList<String> getArrLong() {
        return arrLong;
    }

    public void setArrLong(ArrayList<String> arrLong) {
        this.arrLong = arrLong;
    }
}
