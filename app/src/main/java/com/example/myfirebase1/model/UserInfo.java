package com.example.myfirebase1.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.BR;

public class UserInfo extends BaseObservable {

    private String emailUser;
    private String uidUser;
    private String urlAvatarUser;
    private String introUser;
    private String phoneNumberUser;
    private double totalRunUser;
    private boolean online = false;

    public UserInfo(){}

    @Bindable
    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
        notifyPropertyChanged(BR.emailUser);
    }

    @Bindable
    public String getUidUser() {
        return uidUser;
    }

    public void setUidUser(String uidUser) {
        this.uidUser = uidUser;
        notifyPropertyChanged(BR.uidUser);
    }

    @Bindable
    public double getTotalRunUser() {
        return totalRunUser;
    }

    public void setTotalRunUser(double totalRunUser) {
        this.totalRunUser = totalRunUser;
        notifyPropertyChanged(BR.totalRunUser);
    }

    @Bindable
    public String getIntroUser() {
        return introUser;
    }

    public void setIntroUser(String introUser) {
        this.introUser = introUser;
        notifyPropertyChanged(BR.introUser);
    }

    @Bindable
    public String getPhoneNumberUser() {
        return phoneNumberUser;
    }

    public void setPhoneNumberUser(String phoneNumberUser) {
        this.phoneNumberUser = phoneNumberUser;
        notifyPropertyChanged(BR.phoneNumberUser);
    }

    @Bindable
    public String getUrlAvatarUser() {
        return urlAvatarUser;
    }

    public void setUrlAvatarUser(String urlAvatarUser) {
        this.urlAvatarUser = urlAvatarUser;
        notifyPropertyChanged(BR.urlAvatarUser);
    }

    @BindingAdapter({"android:urlAvatarUser"})
    public static void loadImage(ImageView view,String imgUrl){
        Glide.with(view.getContext())
                .load(imgUrl)
                .circleCrop()
                .into(view);
    }

    @Bindable
    public boolean getOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
        notifyPropertyChanged(BR.online);
    }
}
