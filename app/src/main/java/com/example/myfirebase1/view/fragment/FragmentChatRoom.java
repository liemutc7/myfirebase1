package com.example.myfirebase1.view.fragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Toast;

import com.example.myfirebase1.R;
import com.example.myfirebase1.adapter.ChatAdapter;
import com.example.myfirebase1.adapter.CommentAdapter;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragChatRoomBinding;
import com.example.myfirebase1.model.Chat;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.ChatRoomPresenter;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.view.event.OnChatRoomCallBack;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FragmentChatRoom extends BaseFragment<ChatRoomPresenter, FragChatRoomBinding> implements OnChatRoomCallBack, View.OnClickListener, ValueEventListener, CommentAdapter.ItemClickListener, OnSuccessListener<Void>, OnFailureListener, ChatAdapter.ItemClickListener {

    public static final String TAG = FragmentChatRoom.class.getName();

    private ChatAdapter chatAdapter;
    //    private ArrayList<Chat> data;
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference myRef;
    private FirebaseUser currentUser;
    private ArrayList<Chat> dataChat = new ArrayList<>();


    @Override
    protected ChatRoomPresenter getPresenter() {
        return new ChatRoomPresenter(this);
    }

    @Override
    protected void initView() {
        setTitle("ChatRoom");
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        myRef = mDatabase.getReference("Chats");
        myRef.addValueEventListener(this);
        binding.ivSend.setOnClickListener(this);
        chatAdapter = new ChatAdapter(mContext,currentUser.getEmail());
        binding.lvChat.setAdapter(chatAdapter);
        chatAdapter.setListener(this);
        myAvatar = PrefUtil.getString(mContext, Constances.MY_AVATAR_URL, "");
        myRefUserInfo.addValueEventListener(this);
        hideLoading();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_chat_room;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_send:
                getChatTextAndSend();
                break;
            default:
                break;
        }
    }

    private void getChatTextAndSend() {
        if (binding.edtText.getText().toString().isEmpty() || binding.edtText.getText() == null) {
            return;
        }
        if (currentUser.getEmail().isEmpty()) {
            return;
        }
        if (myAvatar.isEmpty()) {
            return;
        }

        String textChat = binding.edtText.getText().toString();
        String emailChat = currentUser.getEmail();
        Chat chat = new Chat();
        chat.setAvatar(myAvatar);
        chat.setEmail(emailChat);
        chat.setText(textChat);
        myRef.child("" + chat.getId()).setValue(chat).addOnSuccessListener(this)
                .addOnFailureListener(this);

        binding.edtText.setText("");

    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

        if (dataSnapshot.getRef().equals(myRefUserInfo)) {
            showLoading();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                UserInfo info = data.getValue(UserInfo.class);
                for (Chat d : dataChat) {
                    if (info.getEmailUser().equals(d.getEmail())) {
                        myRef.child(d.getId()+"").child("avatar").setValue(info.getUrlAvatarUser());
//                        d.setAvatar(info.getUrlAvatarUser());
                    }
                }
            }
            chatAdapter.setData(dataChat);
            chatAdapter.notifyDataSetChanged();
            hideLoading();
        } else {
            dataChat.clear();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                Chat chat = data.getValue(Chat.class);
                dataChat.add(chat);
            }
            chatAdapter.setData(dataChat);
            binding.lvChat.scrollToPosition(dataChat.size() - 1);
            binding.edtText.setText("");
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void onItemClick(Chat chat) {

    }

    @Override
    public void onLongItemClick(Chat chat) {
        myRef.child(chat.getId() + "").removeValue()
                .addOnFailureListener(this)
                .addOnSuccessListener(this);
    }

    private String spilitEmail(String email) {
        return email.split("@")[0];
    }


    @Override
    public void onSuccess(Void aVoid) {

    }

    @Override
    public void onFailure(@NonNull Exception e) {
        Toast.makeText(mContext, "Fail img_send", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }
}
