package com.example.myfirebase1.view.event;

public interface OnLocationCallBack extends OnCallBackToView{
    void snapShotSuccess(String mPath);

    void snapShotNotSuccess();
}
