package com.example.myfirebase1.view.activity;

import android.content.Intent;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseActivity;
import com.example.myfirebase1.databinding.ActivityMainBinding;
import com.example.myfirebase1.dialog.LogoutDialog;
import com.example.myfirebase1.presenter.ImagePresenter;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.view.event.OnImageCallBack;
import com.example.myfirebase1.view.fragment.FragmentChangeAvatar;
import com.example.myfirebase1.view.fragment.FragmentChatListUser;
import com.example.myfirebase1.view.fragment.FragmentChatRoom;
import com.example.myfirebase1.view.fragment.FragmentLocation;
import com.example.myfirebase1.view.fragment.FragmentLogin;
import com.example.myfirebase1.view.fragment.FragmentNewsFeed;
import com.example.myfirebase1.view.fragment.FragmentRankDistance;
import com.example.myfirebase1.view.fragment.FragmentTest;
import com.example.myfirebase1.view.fragment.FragmentUserInfo;
import com.facebook.BuildConfig;
import com.facebook.LoggingBehavior;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.security.MessageDigest;


public class MainActivity extends BaseActivity<ImagePresenter, ActivityMainBinding>
        implements OnImageCallBack, View.OnClickListener , LogoutDialog.LogoutDialogCallBack {

    public static final String TAG = MainActivity.class.getName();

    public static final String LOCAL = "coordinates";

    private FirebaseStorage storage = FirebaseStorage.getInstance();
    public static final String LOG_OUT = "log.out";
//    private StorageReference storageRef = storage.getReference();
//
    private FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference myRefContainer = mDatabase.getReference();
    private String email;
//    private String realpath = "";
//    private FirebaseAuth mAuth;
//    private FirebaseUser currentUser;
    private OnMainListener onMainListener;

    public void setOnMainListener(OnMainListener onMainListener) {
        this.onMainListener = onMainListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        //initialize Facebook SDK
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        if (BuildConfig.DEBUG) {
//            FacebookSdk.setIsDebugEnabled(true);
//            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
//        }

        super.onCreate(savedInstanceState);
//        mAuth = FirebaseAuth.getInstance();
//        currentUser = mAuth.getCurrentUser();
//        myRefContainer = mDatabase.getReference().child("Users");
        myRefContainer = mDatabase.getReference();
        initView();
    }

    @Override
    protected ImagePresenter getPresenter() {
        return new ImagePresenter(this);
    }

    private void initView() {

        showEmail();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                onBackPressed();
            }
        });

        showFragment(FragmentLogin.TAG, null, null);




    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getContentId() {
        return R.id.frame_layout;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_gps:
                toGps();
                return true;

            case R.id.item_chat_list:
                toChatList();
                return true;


            case R.id.item_log_out:
                LogoutDialog logoutDialog = new LogoutDialog(this);
                logoutDialog.setListener(this);
                logoutDialog.show();
                return true;

            case R.id.item_rank_distance:
                toRankDistance();
                return true;

            case R.id.sub_item_0:
                toChangeAvatar();
                return true;
            case R.id.sub_item_1:
                toStatus();
                return true;
//            case R.id.sub_item_2:
//                Toast.makeText(this, "Sub item 2 click", Toast.LENGTH_SHORT).show();
//                showFragment(FragmentTest.TAG, FragmentNewsFeed.TAG, null);
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void toChatList() {
        showFragment(FragmentChatListUser.TAG,FragmentNewsFeed.TAG,null);
    }

    private void toRankDistance() {
        showFragment(FragmentRankDistance.TAG,FragmentNewsFeed.TAG,null);
    }

    private void toChangeAvatar() {
        showFragment(FragmentChangeAvatar.TAG,FragmentNewsFeed.TAG,null);
    }

    private void showEmail() {
        Intent intent = getIntent();
        if (intent.getStringExtra("email") != null) {
            email = intent.getStringExtra("email");
//            binding.tvEmail.setText(email);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }

    private void toStatus() {
        onMainListener.showStatus(PrefUtil.getBoolean(this, Constances.IS_SHOW_STATUS,true));
    }


    private void toGps() {
        showFragment(FragmentLocation.TAG, FragmentNewsFeed.TAG, null);
    }



    private void toLogOut() {
        Bundle bundle = new Bundle();
        bundle.putString(LOG_OUT, "one");
        PrefUtil.putBoolean(this,Constances.IS_LOG_OUT,true);
        showFragment(FragmentLogin.TAG, null, bundle);
        putOfflineUser();

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onLogOut() {
        toLogOut();
    }


    //    private String getRealPathFromURI(Uri uri) {
//        String path = null;
//        String[] proj = {MediaStore.MediaColumns.DATA};
//        Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
//        if (cursor.moveToFirst()) {
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//            path = cursor.getString(column_index);
//        }
//        cursor.close();
//        return path;
//    }
    public interface OnMainListener {
        void showStatus(Boolean isShow);
//        void clearCurrentUser();
    }

    @Override
    protected void onStop() {
        super.onStop();
        putOfflineUser();

    }

    protected void putOfflineUser(){
        try {
            String myUid = PrefUtil.getString(this,Constances.MY_UID,"");
            if(!myUid.isEmpty()){
                myRefContainer.child("Users").child(myUid).child("online").setValue(false);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}


