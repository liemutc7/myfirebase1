package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnRegisterCallBack;

public class RegisterPresenter extends BasePresenter<OnRegisterCallBack>{
    public RegisterPresenter(OnRegisterCallBack mCallBack) {
        super(mCallBack);
    }
}
