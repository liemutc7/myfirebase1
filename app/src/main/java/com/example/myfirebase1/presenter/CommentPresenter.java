package com.example.myfirebase1.presenter;

import com.example.myfirebase1.model.Image;
import com.example.myfirebase1.view.event.OnCommentCallBack;

import java.util.ArrayList;

public class CommentPresenter extends BasePresenter<OnCommentCallBack>{
    public CommentPresenter(OnCommentCallBack mCallBack) {
        super(mCallBack);
    }
    public void toLike(int count) {
        try {
            mCallBack.showLoading();
            count++;
            mCallBack.toLikeSuccess(count);
        }catch (Exception ex){
            ex.printStackTrace();
            mCallBack.hideLoading();
            mCallBack.toLikeNotSuccess();
        }

    }
}
