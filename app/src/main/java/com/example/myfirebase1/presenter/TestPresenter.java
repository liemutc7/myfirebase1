package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnTestCallBack;

public class TestPresenter extends BasePresenter<OnTestCallBack> {
    public TestPresenter(OnTestCallBack mCallBack) {
        super(mCallBack);
    }
}
