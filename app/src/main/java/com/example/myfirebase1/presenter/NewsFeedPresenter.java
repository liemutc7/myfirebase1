package com.example.myfirebase1.presenter;


import android.os.Bundle;

import com.example.myfirebase1.model.Image;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.view.event.OnNewsFeedCallBack;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

public class NewsFeedPresenter extends BasePresenter<OnNewsFeedCallBack> {

    public NewsFeedPresenter(OnNewsFeedCallBack mCallBack) {
        super(mCallBack);
    }

    public void toLike(ArrayList<Image> dataImage, int position) {
        try {
            mCallBack.showLoading();
            int count = dataImage.get(position).getCountLike();
            boolean like = dataImage.get(position).isLike();
            count++;
            mCallBack.toLikeSuccess(position, count, like);
        } catch (Exception ex) {
            ex.printStackTrace();
            mCallBack.hideLoading();
            mCallBack.toLikeNotSuccess();
        }

    }

    public void toDisLike(ArrayList<Image> dataImage, int position) {
        try {
            mCallBack.showLoading();
            int count = dataImage.get(position).getCountLike();
            boolean like = dataImage.get(position).isLike();
            count--;
            mCallBack.toDisLikeSuccess(position, count, like);
        } catch (Exception ex) {
            ex.printStackTrace();
            mCallBack.hideLoading();
            mCallBack.toLikeNotSuccess();
        }
    }

    public void toComment(long id, String email, int countComment, int countLike, boolean isLike) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString(Constances.ID_IMAGE, "" + id);
            bundle.putString(Constances.EMAIL_IMAGE, "" + email);
            bundle.putInt(Constances.COUNT_COMMENT_IMAGE, countComment);
            bundle.putInt(Constances.COUNT_LIKE_IMAGE, countLike);
            bundle.putBoolean(Constances.IS_LIKE_IMAGE, isLike);
            mCallBack.toCommentSuccess(bundle);
        } catch (Exception ex) {
            ex.printStackTrace();
            mCallBack.hideLoading();
            mCallBack.toCommentNotSuccess();
        }
    }

    public void dataChange(ArrayList<Image> dataImage, DataSnapshot dataSnapshot) {
        mCallBack.showLoading();
        try {
            dataImage.clear();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                Image image = data.getValue(Image.class);
                dataImage.add(image);
            }
            mCallBack.toSuccessDataChange(dataImage);
            mCallBack.hideLoading();
        } catch (Exception ex) {
            ex.printStackTrace();
            mCallBack.toNotSuccessDataChange();
            mCallBack.hideLoading();
        }
    }

    public void dataChangeAvatar(ArrayList<Image> dataImage, DataSnapshot dataSnapshot) {
        mCallBack.showLoading();
        try {
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                UserInfo info = data.getValue(UserInfo.class);
                for (Image d : dataImage) {
                    if (info.getEmailUser().equals(d.getMail())) {
                        mCallBack.toChangeAvatar(info, d);
//                        d.setUrlAvatar(info.getUrlAvatarUser());
                    }
                }
            }
            mCallBack.toSuccessAvatarDataChange(dataImage);
            mCallBack.hideLoading();
        } catch (Exception ex) {
            ex.printStackTrace();
            mCallBack.toNotSuccessAvatarDataChange();
            mCallBack.hideLoading();
        }
    }


}
