package com.example.myfirebase1.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.databinding.ItemRankDistanceBinding;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.utils.CommonUtils;

import java.util.ArrayList;

public class RankDistanceAdapter extends RecyclerView.Adapter<RankDistanceAdapter.ViewHolder> {

    private ArrayList<UserInfo> data;
    private LayoutInflater inflater;
    private Context context;
    private RankListener listener;
    private int number = 1;


    public RankDistanceAdapter(Context context){
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setListener(RankListener listener) {
        this.listener = listener;
    }
    public void setData(ArrayList<UserInfo> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemRankDistanceBinding binding = ItemRankDistanceBinding.inflate(inflater);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bindData(data.get(i), i, listener);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemRankDistanceBinding binding;
        private RankListener listener;

        public ViewHolder(@NonNull ItemRankDistanceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @SuppressLint("SetTextI18n")
        public void bindData(final UserInfo item, final int position, final RankDistanceAdapter.RankListener listener) {
            binding.tvRankNumber.setText(number+"");
            binding.tvRankEmail.setText(CommonUtils.getInstance().spilitEmail(item.getEmailUser()));
            String totalRun = CommonUtils.getInstance().formatDoubleToString(item.getTotalRunUser());
            binding.tvRankTotal.setText(totalRun);

            if(item.getUrlAvatarUser()!=null){
                Glide.with(context)
                        .load(item.getUrlAvatarUser())
                        .centerCrop()
                        .circleCrop()
                        .into(binding.ivRankAvatar);
            }

            binding.rlRank.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        listener.toInfo(item.getEmailUser());
                    }
                }
            });

            number++;

        }

    }
    public interface RankListener {
        void toInfo(String email);
    }
}
