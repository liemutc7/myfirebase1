package com.example.myfirebase1.presenter;

import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.view.event.OnChatListUserCallBack;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

public class ChatListUserPresenter extends BasePresenter<OnChatListUserCallBack>{
    public ChatListUserPresenter(OnChatListUserCallBack mCallBack) {
        super(mCallBack);
    }

    public void toDataChange(ArrayList<UserInfo> dataUser, DataSnapshot dataSnapshot) {
        mCallBack.showLoading();
        try {
            dataUser.clear();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                UserInfo userInfo = data.getValue(UserInfo.class);
                dataUser.add(userInfo);
            }
            mCallBack.toSuccessDataChange(dataUser);
            mCallBack.hideLoading();
        }catch (Exception ex){
            ex.printStackTrace();
            mCallBack.toNotSuccessDataChange();
            mCallBack.hideLoading();
        }
    }
}
