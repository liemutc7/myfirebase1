package com.example.myfirebase1.view.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.View;

import com.example.myfirebase1.CAApplication;
import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragLocationBinding;
import com.example.myfirebase1.presenter.LocationPresenter;
import com.example.myfirebase1.utils.CommonUtils;
import com.example.myfirebase1.view.event.OnLocationCallBack;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class FragmentLocation extends BaseFragment<LocationPresenter, FragLocationBinding> implements OnLocationCallBack,
        OnMapReadyCallback, LocationListener, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener, View.OnClickListener, ValueEventListener {

    public static final String TAG = FragmentLocation.class.getName();


    public static final String DISTANCE_ME = "distance.me";
    public static final String SPEED_ME = "speed.me";
    public static final String PATH_IMAGE = "path.image";
    public static final String LATITUDE_ME = "LATITUDE_ME";
    public static final String LONGITUDE_ME = "LONGITUDE_ME";
    public static final String ARRAY_POINT_LAT = "ARRAY_POINT_LAT";
    public static final String ARRAY_POINT_LONG = "ARRAY_POINT_LONG";

    private GoogleMap mMap;
    private Geocoder geocoder;
    private Marker marker;
    private Marker currentLocationMarker;
    private ArrayList<LatLng> arrayPoints;
    private ArrayList<String> arrayPointsLat;
    private ArrayList<String> arrayPointsLong;
    private PolylineOptions polylineOptions;
    private double totalDistance = 0.0f;
    private double oldSpeed = 0.0f;
    private double oldTotalRunUser = 0.001f;
    private Location oldLocation = null;

//    private BroadcastReceiver broadcastReceiver;
//    private GoogleApiClient googleApiClient;
//    private LatLng lastKnownLatLng;
//    private Polyline gpsTrack;

    private final String[] PERMISSION = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private final String[] PERMISSION_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected LocationPresenter getPresenter() {
        return new LocationPresenter(this);
    }

    @Override
    protected void initView() {
        setTitle("Location");
        if (!CommonUtils.getInstance().isLocationEnabled(mContext)) {
            showNotifyLong(mContext.getString(R.string.you_need_turn_on_gps));
        }
        if (checkPermission() && checkPermissionStorage()) {
            init();
        }
        binding.ivScreenShot.setOnClickListener(this);
        arrayPoints = new ArrayList<>();
        arrayPointsLat = new ArrayList<>();
        arrayPointsLong = new ArrayList<>();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_location;
    }


    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String s : PERMISSION) {
                if (ActivityCompat.checkSelfPermission(mContext, s) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(PERMISSION, 0);
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (checkPermission()) {
            init();
        } else if (checkPermissionStorage()) {
            CaptureMapScreen();
        } else {
            showNotify("You need agree permission");
            getParent().showFragment(FragmentNewsFeed.TAG, null, null);
        }
    }

    private void init() {


//        tvSpeed = findViewById(R.id.tv_speed);

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    @SuppressLint("MissingPermission")
    private void initMap() {
        Places.initialize(CAApplication.getInstance(), getString(R.string.google_maps_key));

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);//cho phep zoom in zoom out map len

        LocationManager manager = (LocationManager) mContext.getSystemService(mContext.LOCATION_SERVICE);
        manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1, 1, this);
        manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                1, 1, this);

        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
        if (mMap != null) {
            mMap.clear();
        }
//        Log.d(TAG, "totalRun to String: " + myRefUserInfo.child(myUid).child("totalRunUser").toString());
//        Log.d(TAG, "totalRun to String: " + myRefUserInfo.child(myUid).child("totalRunUser").getKey());
//        Log.d(TAG, "totalRun to Ref: " + myRefUserInfo.child(myUid).child("totalRunUser").getRef().toString());
        myRefUserInfo.child(myUid).child("totalRunUser").addValueEventListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        geocoder = new Geocoder(mContext);
        initMap();
        hideLoading();
    }


    @Override
    public void onLocationChanged(Location location) {
        LatLng lng = new LatLng(location.getLatitude(), location.getLongitude());
        if (oldLocation == null) {
            oldLocation = location;
        }
        CameraPosition position = new CameraPosition(
                lng, 15, 0, 0);
        //v:zoom
        //v1:tilt:Góc, tính theo độ, của góc camera từ nadir (đối diện trực tiếp với Trái đất)
        //       //. Xem độ nghiêng (float) để biết chi tiết về các hạn chế về phạm vi giá trị
        //       //. phạm vi từ 0 đến 90 độ.
        //v2:bearing://Hướng mà máy ảnh hướng vào, theo độ theo chiều kim đồng hồ từ phía bắc.
        //        // Giá trị này sẽ được chuẩn hóa trong phạm vi bao gồm 0 độ và độc quyền 360 độ
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

        if (location != null) {
            if (currentLocationMarker != null) {
                currentLocationMarker.remove();
            }
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//            LatLngEntity latLngEntity = new LatLngEntity(location.getLatitude(),location.getLongitude());
            arrayPointsLat.add(location.getLatitude() + "");
            arrayPointsLong.add(location.getLongitude() + "");
            MarkerOptions marker = new MarkerOptions();
            marker.position(latLng);
            currentLocationMarker = mMap.addMarker(marker);
            polylineOptions = new PolylineOptions();
            polylineOptions.color(Color.RED);
            polylineOptions.width(5);
            arrayPoints.add(latLng);
            polylineOptions.addAll(arrayPoints);
            mMap.addPolyline(polylineOptions);
//            calculateMiles();
            calculateTotalDistance(oldLocation, location);
            oldLocation = location;
//            binding.meterOne.setProgress(totalDistance);
            String total = CommonUtils.getInstance().formatDoubleToString(totalDistance);
            binding.tvMeasure.setText(total);
            binding.tvLatitude.setText(location.getLatitude() + "");
            binding.tvLongitude.setText(location.getLongitude() + "");
            if (location.getSpeed() > oldSpeed) {
                String maxSp = CommonUtils.getInstance().formatDoubleToString(location.getSpeed());
                binding.tvSpeed.setText(maxSp);
                oldSpeed = location.getSpeed();
            }
        }
    }


    private void calculateTotalDistance(Location oldLat, Location newLat) {
        float distance = newLat.distanceTo(oldLat) / 1000;
        oldTotalRunUser += distance;
        totalDistance += distance;
        myRefUserInfo.child(myUid).child("totalRunUser").setValue(oldTotalRunUser);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_screen_shot:
//                screenShot();
                CaptureMapScreen();

                break;
            default:
                break;
        }
    }


    public void CaptureMapScreen() {
        SnapshotReadyCallback callback = new SnapshotReadyCallback() {


            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                // TODO Auto-generated method stub
                String mPath = "";

                mPresenter.snapShot(snapshot, mPath);


            }
        };

        mMap.snapshot(callback);

        // myMap is object of GoogleMap +> GoogleMap myMap;
        // which is initialized in onCreate() =>
        // myMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_pass_home_call)).getMap();
    }


    @Override
    public void snapShotSuccess(String mPath) {
        Bundle bundle = new Bundle();
        bundle.putString(PATH_IMAGE, "" + mPath);
        bundle.putString(DISTANCE_ME, "" + binding.tvMeasure.getText());
        bundle.putString(SPEED_ME, "" + binding.tvSpeed.getText());
        bundle.putString(LATITUDE_ME, "" + binding.tvLatitude.getText());
        bundle.putString(LONGITUDE_ME, "" + binding.tvLongitude.getText());
        bundle.putStringArrayList(ARRAY_POINT_LAT, arrayPointsLat);
        bundle.putStringArrayList(ARRAY_POINT_LONG, arrayPointsLong);
        getParent().showFragment(FragmentShare.TAG, FragmentLocation.TAG, bundle);
    }

    @Override
    public void snapShotNotSuccess() {
        showNotify("snapShotNotSuccess");
    }


    private boolean checkPermissionStorage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String s : PERMISSION_STORAGE) {
                if (ActivityCompat.checkSelfPermission(mContext, s) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(PERMISSION_STORAGE, 0);
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (marker != null) {
            marker.remove();
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if (marker != null) {
            marker.remove();
        }

        String address = getAdressNameByPosition(latLng);
        marker = drawMarker("LongClick", address, BitmapDescriptorFactory.HUE_BLUE, latLng);
    }

    private String getAdressNameByPosition(LatLng lng) {
        try {
            //truyen vi tri ra list ten duong
            List<Address> arr = geocoder.getFromLocation(lng.latitude, lng.longitude, 1);
            String address = "";
            if (arr.size() > 0) {
                address = arr.get(0).getAddressLine(0);
            }
            return address;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    private Marker drawMarker(String title, String snippet, float hue, LatLng lng) {

        //Polygon//ve khoanh vung
        //Polyline
        //Circle//ve hinh tron

        MarkerOptions options = new MarkerOptions();
        options.position(lng);
        options.icon(BitmapDescriptorFactory.defaultMarker(hue));
        options.title(title);
        options.snippet(snippet);
        return mMap.addMarker(options);
    }


    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }


    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        if (dataSnapshot.getValue() == null || dataSnapshot.getValue() instanceof Long) {
            myRefUserInfo.child(myUid).child("totalRunUser").setValue(oldTotalRunUser);
        } else {
            oldTotalRunUser = (double) dataSnapshot.getValue();
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
