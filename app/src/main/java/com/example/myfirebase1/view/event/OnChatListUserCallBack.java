package com.example.myfirebase1.view.event;

import com.example.myfirebase1.model.UserInfo;

import java.util.ArrayList;

public interface OnChatListUserCallBack extends OnCallBackToView{
    void toSuccessDataChange(ArrayList<UserInfo> dataUser);

    void toNotSuccessDataChange();
}
