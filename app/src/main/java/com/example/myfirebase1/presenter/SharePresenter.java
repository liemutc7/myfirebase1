package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnShareCallBack;

public class SharePresenter extends BasePresenter<OnShareCallBack>{
    public SharePresenter(OnShareCallBack mCallBack) {
        super(mCallBack);
    }
}
