package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnChatListCallBack;

public class ChatListPresenter extends BasePresenter<OnChatListCallBack>{
    public ChatListPresenter(OnChatListCallBack mCallBack) {
        super(mCallBack);
    }
}
