package com.example.myfirebase1.presenter;

import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.view.event.OnUserInfoCallBack;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

public class UserInfoPresenter extends BasePresenter<OnUserInfoCallBack>{
    public UserInfoPresenter(OnUserInfoCallBack mCallBack) {
        super(mCallBack);
    }

    public void dataUserInfo(ArrayList<UserInfo> dataUserInfo, DataSnapshot dataSnapshot) {
        mCallBack.showLoading();
        try {
            dataUserInfo.clear();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                UserInfo userInfo = data.getValue(UserInfo.class);
                dataUserInfo.add(userInfo);
            }
            mCallBack.toSuccessDataChange(dataUserInfo);
            mCallBack.hideLoading();
        }catch (Exception ex){
            ex.printStackTrace();
            mCallBack.toNotSuccessDataChange();
            mCallBack.hideLoading();
        }
    }
}
