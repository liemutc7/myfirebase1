package com.example.myfirebase1.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.databinding.ItemChatBinding;

import com.example.myfirebase1.model.Chat;
import com.example.myfirebase1.utils.CommonUtils;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private ArrayList<Chat> data;
    private LayoutInflater inflater;
    private ItemClickListener listener;

    public CommentAdapter(Context context) {
        inflater = LayoutInflater.from(context);
//        this.data = data;
    }

    public void setData(ArrayList<Chat> data) {
        this.data = data;
    }

    public void setListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemChatBinding binding = ItemChatBinding.inflate(inflater);
        return new CommentAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.bindData(data.get(i));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(listener!=null){
                    listener.onItemClick(data.get(i));
                }

            }
        });
        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(listener!=null){
                    listener.onLongItemClick(data.get(i));
                }

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0: data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemChatBinding binding;

        public ViewHolder(@NonNull ItemChatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(Chat item) {
            binding.tvEmailChat.setText(CommonUtils.getInstance().spilitEmail(item.getEmail()));
            binding.tvTextChat.setText(item.getText());
            Glide.with(itemView.getContext())
                    .load(item.getAvatar())
                    .circleCrop()
                    .into(binding.ivAvatar);
        }
    }

    public interface ItemClickListener{
        void onItemClick(Chat chat);
        void onLongItemClick(Chat chat);
    }
}
