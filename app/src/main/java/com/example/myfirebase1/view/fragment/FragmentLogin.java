package com.example.myfirebase1.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.ViewStructure;
import android.widget.Toast;

import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragLoginBinding;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.LoginPresenter;
import com.example.myfirebase1.utils.CommonUtils;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.utils.TextUtils;
import com.example.myfirebase1.view.activity.MainActivity;
import com.example.myfirebase1.view.event.OnLoginCallBack;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class FragmentLogin extends BaseFragment<LoginPresenter, FragLoginBinding> implements OnLoginCallBack, View.OnClickListener {

    public static final String TAG = FragmentLogin.class.getName();
    public static final String EMAIL = "email";
    public static final String UID = "uid";
    private CallbackManager callbackManager;

    private static final int REQUEST_LOGIN = 1337;
    private FirebaseAuth mAuth;

    @Override
    protected LoginPresenter getPresenter() {
        return new LoginPresenter(this);
    }

    @Override
    protected void initView() {

        binding.loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(mContext,"Successful", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onCancel() {
                Toast.makeText(mContext,"Login attempt canceled.", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onError(FacebookException e) {
                Toast.makeText(mContext,"Login attempt failed.", Toast.LENGTH_LONG).show();
            }

        });

        TextUtils.getInstance().setTextColor(binding.btnLogin);
        mAuth = FirebaseAuth.getInstance();
        binding.btnLogin.setOnClickListener(this);
        binding.tvRegister.setOnClickListener(this);
        loadCurrentUser();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected int getLayoutId() {
        FacebookSdk.sdkInitialize(getParent().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        return R.layout.frag_login;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        putOfflineUser();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                showLoading();
                login();
                break;
            case R.id.tv_register:
//                Intent intent = new Intent(this, FragmentRegister.class);
//                startActivityForResult(intent, REQUEST_LOGIN);
                getParent().showFragment(FragmentRegister.TAG, FragmentLogin.TAG, null);
                break;
            default:
                break;
        }
    }


    private void loadCurrentUser() {

        if (getArguments() != null) {
            if (getArguments().getString(MainActivity.LOG_OUT) != null) {
                return;
            }
        }

        if (getArguments() != null) {
            if (getArguments().getString(FragmentRegister.EXTRA_PASS, null) != null &&
                    getArguments().getString(FragmentRegister.EXTRA_USER, null) != null) {
                binding.edtEmail.setText(getArguments().getString(FragmentRegister.EXTRA_USER));
                binding.edtPassword.setText(getArguments().getString(FragmentRegister.EXTRA_PASS));
                if(mCurrentUser!=null){
                    UserInfo userInfo = new UserInfo();
                    userInfo.setEmailUser(mCurrentUser.getEmail());
                    userInfo.setUidUser(mCurrentUser.getUid());
                    userInfo.setUrlAvatarUser(Constances.IMAGE_AVATAR_DEFAULT);
                    myRefUserInfo.child(mCurrentUser.getUid()).setValue(userInfo);
                }
                return;
            }
        }

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null && !PrefUtil.getBoolean(mContext, Constances.IS_LOG_OUT, false)) {
            Bundle bundle = new Bundle();
            bundle.putString(EMAIL, currentUser.getEmail());
            bundle.putString(UID, currentUser.getUid());
            getParent().showFragment(FragmentNewsFeed.TAG, null, bundle);
        }

    }


    private void login() {
        try {
            if (binding.edtEmail.getText().toString().isEmpty() || binding.edtPassword.getText().toString().isEmpty()){
                hideLoading();
                return;

            }
            final String email = binding.edtEmail.getText().toString();
            String password = binding.edtPassword.getText().toString();
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
//                                showNotify("Login success");
                                showSnackbar(binding.rlContainerLogin,"Login success");
                                Log.d(TAG, "signInWithEmail:success");
                                getParent().showFragment(FragmentNewsFeed.TAG, null, null);
                                hideLoading();

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.e(TAG, "signInWithEmail:failure", task.getException());
//                                showNotify("Login fail");
                                showSnackbar(binding.rlContainerLogin,"Login fail");
                                hideLoading();
                            }

                        }
                    });
        } catch (Exception ex) {
            ex.printStackTrace();
            hideLoading();
        }
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }


}
