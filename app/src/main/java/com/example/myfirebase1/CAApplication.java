package com.example.myfirebase1;

import android.app.Application;

public class CAApplication extends Application {
    private static final String TAG = CAApplication.class.getName();

    private static CAApplication instance;

    public static CAApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
