package com.example.myfirebase1.base;

import android.content.Context;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myfirebase1.R;
import com.example.myfirebase1.model.Image;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.BasePresenter;
import com.example.myfirebase1.utils.CommonUtils;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.view.activity.MainActivity;
import com.example.myfirebase1.view.fragment.FragmentNewsFeed;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public abstract class BaseFragment<T extends BasePresenter, BD extends ViewDataBinding> extends Fragment {

    public static final String TAG = BaseFragment.class.getName();

    protected FirebaseStorage storage = FirebaseStorage.getInstance();
    protected StorageReference storageRef = storage.getReference();
    protected FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    protected DatabaseReference myRef = mDatabase.getReference();
    protected DatabaseReference myRefUserInfo = mDatabase.getReference();
    protected FirebaseAuth mAuth;
    protected FirebaseUser mCurrentUser;

    protected T mPresenter;
    protected BD binding;
    protected Context mContext;
    protected View mRootView;

    protected String myAvatar = "";
    protected String myEmail;
    protected String myUid;
    protected String myDisplay;

    protected onBaseFragmentClick listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        mContext = getActivity();
        mPresenter = getPresenter();
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        myRef = mDatabase.getReference("Images");
        myRefUserInfo = mDatabase.getReference("Users");

        if (mCurrentUser != null) {
            myEmail = mCurrentUser.getEmail();
            myUid = mCurrentUser.getUid();
            myDisplay = mCurrentUser.getDisplayName();
        }

        initView();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        putOnlineUser();
    }

    protected void putOnlineUser() {
        try {
            String myUid = PrefUtil.getString(mContext, Constances.MY_UID, "");
            if (!myUid.isEmpty()) {
                myRefUserInfo.child(myUid).child("online").setValue(true);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void putOfflineUser() {
        try {
            String myUid = PrefUtil.getString(mContext, Constances.MY_UID, "");
            if (!myUid.isEmpty()) {
                myRefUserInfo.child(myUid).child("online").setValue(false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setListener(onBaseFragmentClick listener) {
        this.listener = listener;
    }

    protected abstract T getPresenter();

    protected abstract void initView();

    protected abstract int getLayoutId();

    protected void setTitle(String title) {
        Objects.requireNonNull(getActivity()).setTitle(title);
    }

    public <G extends View> G findViewById(int idView) {
//        G view = mRootView.findViewById(idView);
        return findViewById(idView, null);
    }

    public <G extends View> G findViewById(int idView, View.OnClickListener event) {
        G view = mRootView.findViewById(idView);
        if (event != null) {
            view.setOnClickListener(event);
        }
        return view;
    }

    protected MainActivity getParent() {
        return (MainActivity) getActivity();
    }

    protected void showNotify(String text) {
        Toast.makeText(mContext, "" + text, Toast.LENGTH_SHORT).show();
    }

    protected void showNotify(int text) {
        Toast.makeText(mContext, "" + text, Toast.LENGTH_SHORT).show();
    }

    protected void showNotifyLong(String text) {
        Toast.makeText(mContext, "" + text, Toast.LENGTH_LONG).show();
    }

    protected void showNotifyLong(int text) {
        Toast.makeText(mContext, "" + text, Toast.LENGTH_LONG).show();
    }

    protected void showSnackbar(View view,String text){
        Snackbar snackbar = Snackbar
                .make(view, ""+text, Snackbar.LENGTH_SHORT);

        snackbar.show();
    }

    protected void showSnackbarLong(View view,String text){
        Snackbar snackbar = Snackbar
                .make(view, ""+text, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    protected void SendImageStatus(final Boolean isChooseImage, final ImageView ivInputImage, final EditText edtImageName) {
        if (!isChooseImage && (edtImageName.getText().toString().isEmpty() || edtImageName.getText() == null)) {
            showNotify("Please again.Empty image or text...");
            if (listener != null) {
                listener.onHideLoading();
            }
            return;
        }

        final String date = CommonUtils.getInstance().getDate();

        Calendar calendar = Calendar.getInstance();//lay thoi gian he thong
        StorageReference mountainsRef = storageRef.child("image" + calendar.getTimeInMillis() + ".png");

        ivInputImage.setDrawingCacheEnabled(true);
        ivInputImage.buildDrawingCache();

        //check image error
        try {
            Bitmap bitmap = ((BitmapDrawable) ivInputImage.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, baos);
            byte[] data = baos.toByteArray();

            UploadTask uploadTask = mountainsRef.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    showNotify("onFailure uploadTask");
                    if (listener != null) {
                        listener.onHideLoading();
                    }
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    showNotify("onSuccess uploadTask");
                    Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();

                    uri.addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            Uri url;
                            url = task.getResult();
                            Log.i(TAG, "Image uri : " + url);
                            Image image = new Image();
                            image.setDate(date);
                            image.setMail(mCurrentUser.getEmail());
                            image.setUid(mCurrentUser.getUid());
                            if (edtImageName.getText().toString().isEmpty() || edtImageName.getText() != null) {
                                image.setName(edtImageName.getText().toString());
                            }
                            if (isChooseImage) {
                                image.setUrl(url.toString());
                            }
                            myRef.child("" + image.getId()).setValue(image, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    if (databaseError == null) {
                                        showNotify("Success to save dataImage");
                                        ivInputImage.setImageResource(R.drawable.ic_camera_36dp);
                                        edtImageName.setText("");
                                    } else {
                                        showNotify("Fail to save dataImage");
                                    }
                                    if (listener != null) {
                                        listener.onHideLoading();
                                    }
                                }
                            });
                        }
                    });
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
            Image image = new Image();
            image.setDate(date);
            image.setUid(mCurrentUser.getUid());
            image.setMail(mCurrentUser.getEmail());
            if (edtImageName.getText().toString().isEmpty() || edtImageName.getText() != null) {
                image.setName(edtImageName.getText().toString());
            }
            myRef.child("" + image.getId()).setValue(image, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if (databaseError == null) {
                        showNotify("Success to save dataImage");
                        edtImageName.setText("");
                    } else {
                        showNotify("Fail to save dataImage");
                    }
                }
            });
        }
    }


    protected void SendImageLocationStatus(final Boolean isChooseImage, final ImageView ivInputImage,
                                           final EditText edtImageName, final String totalDistance,
                                           final String maxSpeed, final String latitude, final String longitude,
                                           final ArrayList<String> arrLat,
                                           final ArrayList<String> arrLong) {
        if (!isChooseImage && (edtImageName.getText().toString().isEmpty() || edtImageName.getText() == null)) {
            showNotify("Please again.Empty image or text...");
            if (listener != null) {
                listener.onHideLoading();
            }
            return;
        }

        final String date = CommonUtils.getInstance().getDate();

        Calendar calendar = Calendar.getInstance();//lay thoi gian he thong
        StorageReference mountainsRef = storageRef.child("image" + calendar.getTimeInMillis() + ".png");

        ivInputImage.setDrawingCacheEnabled(true);
        ivInputImage.buildDrawingCache();

        //check image error
        try {
            Bitmap bitmap = ((BitmapDrawable) ivInputImage.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, baos);
            byte[] data = baos.toByteArray();

            UploadTask uploadTask = mountainsRef.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    showNotify("onFailure uploadTask");
                    if (listener != null) {
                        listener.onHideLoading();
                    }
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
//                    showNotify("onSuccess uploadTask");
                    Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();

                    uri.addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            Uri url;
                            url = task.getResult();
                            Log.i(TAG, "Image uri : " + url);
                            Image image = new Image();
                            image.setDate(date);
                            image.setMail(mCurrentUser.getEmail());
                            image.setUid(mCurrentUser.getUid());
                            image.setTotalDistance(totalDistance);
                            image.setMaxSpeed(maxSpeed);
                            image.setLatitude(latitude);
                            image.setLongitude(longitude);

                            if (edtImageName.getText().toString().isEmpty() || edtImageName.getText() != null) {
                                image.setName(edtImageName.getText().toString());
                            }
                            if (isChooseImage) {
                                image.setUrl(url.toString());
                            }
                            myRef.child("" + image.getId()).setValue(image, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    if (databaseError == null) {

                                        showNotify("Success to save Image");
                                        ivInputImage.setImageResource(R.drawable.ic_camera_36dp);
                                        edtImageName.setText("");
                                    } else {
                                        showNotify("Fail to save dataImage");
                                    }
                                    if (listener != null) {
                                        listener.onHideLoading();
                                    }
                                }
                            });

                            if (arrLat != null && arrLong != null) {

                                for (int i = 0; i < arrLat.size(); i++) {
                                    myRef.child("" + image.getId()).child("arrLat").child(i + "").setValue(arrLat.get(i));
                                    myRef.child("" + image.getId()).child("arrLong").child(i + "").setValue(arrLong.get(i));

                                }
                            }

                        }
                    });
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
            Image image = new Image();
            image.setDate(date);
            image.setUid(mCurrentUser.getUid());
            image.setMail(mCurrentUser.getEmail());
            if (edtImageName.getText().toString().isEmpty() || edtImageName.getText() != null) {
                image.setName(edtImageName.getText().toString());
            }
            myRef.child("" + image.getId()).setValue(image, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if (databaseError == null) {
                        showNotify("Success to save Image");
                        edtImageName.setText("");
                    } else {
                        showNotify("Fail to save dataImage");
                    }
                }
            });
        }
    }


    public interface onBaseFragmentClick {
        void onHideLoading();
    }

    protected void SendImageAvatar(final Boolean isChooseImage, final ImageView ivInputImage) {
        if (!isChooseImage) {
            showNotify("Please again.Empty image...");
            listener.onHideLoading();
            return;
        }

        Calendar calendar = Calendar.getInstance();//lay thoi gian he thong
        StorageReference mountainsRef = storageRef.child("image" + calendar.getTimeInMillis() + ".png");

        ivInputImage.setDrawingCacheEnabled(true);
        ivInputImage.buildDrawingCache();

        //check image error
        try {
            Bitmap bitmap = ((BitmapDrawable) ivInputImage.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, baos);
            byte[] data = baos.toByteArray();

            UploadTask uploadTask = mountainsRef.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    showNotify("onFailure uploadTask");
                    if (listener != null) {
                        listener.onHideLoading();
                    }
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
//                    showNotify("onSuccess uploadTask");
                    Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();

                    uri.addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            Uri url;
                            url = task.getResult();
                            Log.i(TAG, "Image uri : " + url);
                            UserInfo userInfo = new UserInfo();
//                            image.setDate(date);
                            userInfo.setEmailUser(mCurrentUser.getEmail());
                            userInfo.setUidUser(mCurrentUser.getUid());
                            if (isChooseImage) {
                                userInfo.setUrlAvatarUser(url.toString());
                            }
                            myRefUserInfo.child(mCurrentUser.getUid()).setValue(userInfo, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    if (databaseError == null) {
                                        showNotify("Success to change avatar");
//                                        ivInputImage.setImageResource(R.drawable.ic_camera_36dp);
//                                        edtImageName.setText("");
                                    } else {
                                        showNotify("Fail to save dataImage");
                                    }
                                    listener.onHideLoading();
                                    getParent().showFragment(FragmentNewsFeed.TAG, null, null);

                                }
                            });
                        }
                    });
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
            Image image = new Image();
            image.setMail(mCurrentUser.getEmail());
            image.setUid(mCurrentUser.getUid());
            myRef.child("" + image.getId()).setValue(image, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if (databaseError == null) {
                        showNotify("Success to save Image");
//                        edtImageName.setText("");
                    } else {
                        showNotify("Fail to save Image");
                    }

                }
            });
            listener.onHideLoading();
        }

    }


}
