package com.example.myfirebase1.utils;

public interface Constances {
    String ID_IMAGE = "id.image";
    String EMAIL_IMAGE = "email.image";
    String COUNT_COMMENT_IMAGE = "count.comment.image";
    String COUNT_LIKE_IMAGE = "count.like.image";

    String IS_SHOW_STATUS = "is.show.status";

    String USER_MAIL_DIFF = "user.mail.diff";
    String USER_UID_DIFF = "user.uid.diff";
    String USER_AVATAR_URL_DIFF = "USER_AVATAR_URL_DIFF";

    String CHAT_LIST_MAIL = "chat.list.mail";
    String CHAT_LIST_UID = "chat.list.uid";
    String MY_AVATAR_URL = "my.avatar.url";
    String MY_UID = "my.uid";
    String CHAT_LIST_AVATAR_URL = "CHAT_LIST_AVATAR_URL";
    String IS_LOG_OUT = "IS_LOG_OUT";
    String NEWS_FEED_IMAGE = "NEWS_FEED_IMAGE";
    String IMAGE_LATITUDE = "IMAGE_LATITUDE";
    String IMAGE_LONGITUDE = "IMAGE_LONGITUDE";
    String SERI_ITEM_IMAGE_DIFF = "SERI_ITEM_IMAGE_DIFF";
    String IMAGE_ARRAY_POINT_LAT = "IMAGE_ARRAY_POINT_LAT";
    String IMAGE_ARRAY_POINT_LONG = "IMAGE_ARRAY_POINT_LONG";
    String IMAGE_AVATAR_DEFAULT = "https://firebasestorage.googleapis.com/v0/b/myfirebase1-7a802.appspot.com/o/image1567658329523.png?alt=media&token=68e92652-7327-4b21-aad6-c25a7690742b";

    String IS_LIKE_IMAGE = "IS_LIKE_IMAGE";
}
