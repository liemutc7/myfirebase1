package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnImageReviewCallBack;

public class ImageReviewPresenter extends BasePresenter<OnImageReviewCallBack>{
    public ImageReviewPresenter(OnImageReviewCallBack mCallBack) {
        super(mCallBack);
    }
}
