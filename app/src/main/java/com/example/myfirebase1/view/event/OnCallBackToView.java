package com.example.myfirebase1.view.event;

public interface OnCallBackToView {
    void showLoading();
    void hideLoading();
}
