package com.example.myfirebase1.presenter;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.example.myfirebase1.view.event.OnLocationCallBack;

import java.io.File;
import java.io.FileOutputStream;

public class LocationPresenter extends BasePresenter<OnLocationCallBack>{

    private static final String TAG = LocationPresenter.class.getName();

    public LocationPresenter(OnLocationCallBack mCallBack) {
        super(mCallBack);
    }

    public void snapShot(Bitmap snapshot,String mPath) {
        mCallBack.showLoading();
        Bitmap bitmap = snapshot;
        try {
            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + System.currentTimeMillis();
            File outputDir= new File(path);
            outputDir.mkdirs();
            String pathFile = path + File.separator + "test.png";
            File newFile = new File(pathFile);
//                    FileOutputStream out = new FileOutputStream("/mnt/sdcard/"
//                            + "MyMapScreen" + System.currentTimeMillis()
//                            + ".png");
            FileOutputStream out = new FileOutputStream(newFile);

            // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement

            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            Log.d(TAG,"path : "+path);
            Log.d(TAG,"path File : "+pathFile);
            mPath = newFile.getAbsolutePath();
            mCallBack.snapShotSuccess(mPath);
            mCallBack.hideLoading();
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.hideLoading();
            mCallBack.snapShotNotSuccess();
        }
    }
}
