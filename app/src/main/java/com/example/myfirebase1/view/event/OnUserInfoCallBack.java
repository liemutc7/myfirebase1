package com.example.myfirebase1.view.event;

import com.example.myfirebase1.model.UserInfo;

import java.util.ArrayList;

public interface OnUserInfoCallBack extends OnCallBackToView{
    void toSuccessDataChange(ArrayList<UserInfo> dataUserInfo);

    void toNotSuccessDataChange();
}
