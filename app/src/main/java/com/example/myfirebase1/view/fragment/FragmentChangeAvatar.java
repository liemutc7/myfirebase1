package com.example.myfirebase1.view.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragChangeAvatarBinding;
import com.example.myfirebase1.presenter.ChangeAvatarPresenter;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.view.event.OnChangeAvatarCallBack;

import java.io.FileNotFoundException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;

public class FragmentChangeAvatar extends BaseFragment<ChangeAvatarPresenter, FragChangeAvatarBinding> implements OnChangeAvatarCallBack, View.OnClickListener, BaseFragment.onBaseFragmentClick {

    public static final String TAG = FragmentChangeAvatar.class.getName();
    private static final int REQUEST_CODE_IMAGE = 1888;
    private boolean isChooseImage;


    @Override
    protected ChangeAvatarPresenter getPresenter() {
        return new ChangeAvatarPresenter(this);
    }

    @Override
    protected void initView() {
        setTitle("Change Avatar");
        isChooseImage = false;
        myAvatar = PrefUtil.getString(mContext, Constances.MY_AVATAR_URL,"");
        if (!myAvatar.isEmpty()) {
            Glide.with(mContext)
                    .load(myAvatar)
                    .circleCrop()
                    .into(binding.ivAvatar);
        }
        binding.ivAvatar.setOnClickListener(this);
        binding.btnChangeAvatar.setOnClickListener(this);
        setListener(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_change_avatar;
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_avatar:
                toImageGallery();
                break;
            case R.id.btn_change_avatar:
                SendImageOrStatus();
                break;
            default:
                break;
        }
    }

    private void SendImageOrStatus() {
        showLoading();
        SendImageAvatar(isChooseImage, binding.ivAvatar);
    }

    private void toImageGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
//            realpath = getRealPathFromURI(uri);
            try {
                InputStream inputStream = mContext.getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                binding.ivAvatar.setImageBitmap(bitmap);
                isChooseImage = true;
            } catch (FileNotFoundException e) {
                isChooseImage = false;
                e.printStackTrace();
            }
        } else {
            isChooseImage = false;
        }
    }

    @Override
    public void onHideLoading() {
        hideLoading();
    }
}
