package com.example.myfirebase1.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import com.example.myfirebase1.R;
import com.example.myfirebase1.databinding.ItemUserChatBinding;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.utils.CommonUtils;

import java.util.ArrayList;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {

    private ArrayList<UserInfo> data;
    private LayoutInflater inflater;
    private ItemChatListClickListener listener;

    public ChatListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
//        this.data = data;
    }

    public void setData(ArrayList<UserInfo> data) {
        this.data = data;
    }

    public void setListener(ItemChatListClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemUserChatBinding binding = ItemUserChatBinding.inflate(inflater);
        return new ChatListAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.bindData(data.get(i));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener != null) {
                    listener.onItemClick(data.get(i));
                }

            }
        });
        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (listener != null) {
                    listener.onLongItemClick(data.get(i));
                }

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemUserChatBinding binding;

        public ViewHolder(@NonNull ItemUserChatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(UserInfo item) {
            binding.tvMail.setText(CommonUtils.getInstance().spilitEmail(item.getEmailUser()));
            Glide.with(itemView.getContext())
                    .load(item.getUrlAvatarUser())
                    .circleCrop()
                    .into(binding.ivAvatar);
            if (item.getOnline()) {
                Glide.with(itemView.getContext())
                        .load(R.drawable.icon_blue)
                        .circleCrop()
                        .into(binding.ivCheckOnline);
            } else {
                Glide.with(itemView.getContext())
                        .load(R.drawable.icon_gray)
                        .circleCrop()
                        .into(binding.ivCheckOnline);
            }

        }
    }

    public interface ItemChatListClickListener {
        void onItemClick(UserInfo chat);

        void onLongItemClick(UserInfo chat);
    }
}
