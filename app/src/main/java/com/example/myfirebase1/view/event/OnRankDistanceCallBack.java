package com.example.myfirebase1.view.event;

import com.example.myfirebase1.model.UserInfo;

import java.util.ArrayList;

public interface OnRankDistanceCallBack extends OnCallBackToView{
    void toSuccessRankDataChange(ArrayList<UserInfo> dataInfo);

    void toNotSuccessRankDataChange();
}
