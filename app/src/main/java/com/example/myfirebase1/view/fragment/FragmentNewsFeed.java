package com.example.myfirebase1.view.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.R;
import com.example.myfirebase1.adapter.NewsFeedAdapter;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragNewsFeedBinding;
import com.example.myfirebase1.model.Image;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.NewsFeedPresenter;
import com.example.myfirebase1.utils.CommonUtils;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.PrefUtil;
import com.example.myfirebase1.view.activity.MainActivity;
import com.example.myfirebase1.view.event.OnNewsFeedCallBack;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

import static android.app.Activity.RESULT_OK;

public class FragmentNewsFeed extends BaseFragment<NewsFeedPresenter, FragNewsFeedBinding> implements OnNewsFeedCallBack,
        NewsFeedAdapter.ImageListener, ValueEventListener,
        OnSuccessListener<Void>, OnFailureListener, View.OnClickListener, MainActivity.OnMainListener, BaseFragment.onBaseFragmentClick {

    public static final String TAG = FragmentNewsFeed.class.getName();

    private static final int REQUEST_CODE_IMAGE = 117;
    public static final String MAIL_USER = "mail.user";
    private static int firstNumber = 0;


    //    private FirebaseStorage storage = FirebaseStorage.getInstance();
//    private StorageReference storageRef = storage.getReference();
//
//    private FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
//    private DatabaseReference myRef = mDatabase.getReference();
    private String realpath = "";
//    private FirebaseAuth mAuth;
//    private FirebaseUser mCurrentUser;

    private NewsFeedAdapter newsFeedAdapter;
    private ArrayList<Image> dataImage;

    private boolean isChooseImage = false;


    @Override
    protected NewsFeedPresenter getPresenter() {
        return new NewsFeedPresenter(this);
    }


    @Override
    protected void initView() {
        setTitle("NewsFeed");
        showLoading();
        PrefUtil.putBoolean(mContext, Constances.IS_LOG_OUT, false);
        binding.ivInputImage.setOnClickListener(this);
        binding.ivSaveImage.setOnClickListener(this);
        newsFeedAdapter = new NewsFeedAdapter(mContext);
        binding.lvNewsFeed.setAdapter(newsFeedAdapter);
        dataImage = new ArrayList<>();
//        mAuth = FirebaseAuth.getInstance();
//        mCurrentUser = mAuth.getCurrentUser();
//        myRef = mDatabase.getReference("Images");
        myRef.addValueEventListener(this);
        myRefUserInfo.addValueEventListener(this);
        newsFeedAdapter.setImageListener(this);
        isChooseImage = false;
        binding.tvMail.setText(CommonUtils.getInstance().spilitEmail(mCurrentUser.getEmail()));

        getParent().setOnMainListener(this);
        if (binding.lnContent.getVisibility() == View.VISIBLE) {
            PrefUtil.putBoolean(mContext, Constances.IS_SHOW_STATUS, true);
        } else if (binding.lnContent.getVisibility() == View.GONE) {
            PrefUtil.putBoolean(mContext, Constances.IS_SHOW_STATUS, false);
        }
        PrefUtil.putString(mContext, Constances.MY_UID, myUid);

        setListener(this);
        binding.trMail.setOnClickListener(this);
    }

    private void getValueMyAvatar() {
        myRefUserInfo.child(myUid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserInfo myInfo = dataSnapshot.getValue(UserInfo.class);
                if (myInfo != null) {
                    myAvatar = myInfo.getUrlAvatarUser();
                    PrefUtil.putString(mContext, Constances.MY_AVATAR_URL, myAvatar);
                    try {
                        Glide.with(mContext)
                                .load(myAvatar)
                                .circleCrop()
                                .into(binding.ivMailAvatar);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showNotify("databaseError..load my avatar");
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_news_feed;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }


    @Override
    public void onResume() {
        super.onResume();

        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        }
        CommonUtils.getInstance().changeStatusBar(getActivity(), R.color.ColorSplash);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Glide.with(mContext)
                .load(myAvatar)
                .into(binding.ivMailAvatar);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    private void SendImageOrStatus() {
        showLoading();
        SendImageStatus(isChooseImage, binding.ivInputImage, binding.edtImageName);
    }


    @Override
    public void toLike(int position) {
        mPresenter.toLike(dataImage, position);
    }

    @Override
    public void toDisLike(int position) {
        mPresenter.toDisLike(dataImage, position);
    }

    @Override
    public void toLikeSuccess(int position, int count, boolean like) {
        myRef.child(dataImage.get(position).getId() + "").child("countLike").setValue(count);
        myRef.child(dataImage.get(position).getId() + "").child("like").setValue(!like);
    }

    @Override
    public void toDisLikeSuccess(int position, int count, boolean like) {
        myRef.child(dataImage.get(position).getId() + "").child("countLike").setValue(count);
        myRef.child(dataImage.get(position).getId() + "").child("like").setValue(like);
    }

    @Override
    public void toLikeNotSuccess() {
        showNotify("To Like not Success");
    }

    @Override
    public void toComment(int position) {
//        showNotify("Show comment");
//        Log.d(TAG,"comments id "+dataImage.get(position).getId()+" email "+mCurrentUser.getEmail()+" pos "+dataImage.get(position).getCountComment());
        mPresenter.toComment(dataImage.get(position).getId(),
                mCurrentUser.getEmail(),
                dataImage.get(position).getCountComment(),
                dataImage.get(position).getCountLike(),
                dataImage.get(position).isLike()
        );
    }

    @Override
    public void toImage(Image item) {
        Bundle bundle = new Bundle();
        bundle.putString(Constances.NEWS_FEED_IMAGE, item.getUrl());
        getParent().showFragment(FragmentImageReview.TAG, FragmentNewsFeed.TAG, bundle);
    }

    @Override
    public void toImageLocation(String latitude, String longitude) {
        Bundle bundle = new Bundle();
        bundle.putString(Constances.IMAGE_LATITUDE, latitude);
        bundle.putString(Constances.IMAGE_LONGITUDE, longitude);
        getParent().showFragment(FragmentLocationReview.TAG, FragmentNewsFeed.TAG, bundle);
    }

    @Override
    public void toImageLocationArrayPoint(String latitude, String longitude, ArrayList<String> arrLat, ArrayList<String> arrLong) {
        Bundle bundle = new Bundle();
        bundle.putString(Constances.IMAGE_LATITUDE, latitude);
        bundle.putString(Constances.IMAGE_LONGITUDE, longitude);
        bundle.putStringArrayList(Constances.IMAGE_ARRAY_POINT_LAT, arrLat);
        bundle.putStringArrayList(Constances.IMAGE_ARRAY_POINT_LONG, arrLong);
        getParent().showFragment(FragmentLocationReview.TAG, FragmentNewsFeed.TAG, bundle);
    }

    @Override
    public void toUserInfo(Image item) {
        Bundle bundle = new Bundle();
        bundle.putString(Constances.USER_UID_DIFF, item.getUid());
        bundle.putString(Constances.USER_MAIL_DIFF, item.getMail());
        bundle.putString(Constances.USER_AVATAR_URL_DIFF, item.getUrlAvatar());
        getParent().showFragment(FragmentUserInfo.TAG, FragmentNewsFeed.TAG, bundle);
    }

    @Override
    public void toCommentSuccess(Bundle bundle) {
        getParent().showFragment(FragmentComment.TAG, FragmentNewsFeed.TAG, bundle);
    }


    @Override
    public void toCommentNotSuccess() {
        showNotify("To Comment not Success");
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

        if (dataSnapshot.getRef().equals(myRef)) {
            mPresenter.dataChange(dataImage, dataSnapshot);
        } else {
//            showNotify("Change Avatar ...");
            mPresenter.dataChangeAvatar(dataImage, dataSnapshot);
//            PrefUtil.putString(mContext,Constances.MY_AVATAR_URL,myAvatar);
//            showNotify("Avatar : "+myAvatar);
//            int b = 999;
        }


    }

    @Override
    public void toSuccessDataChange(ArrayList<Image> dataImage) {
        Collections.reverse(dataImage);
        newsFeedAdapter.setData(dataImage);
        newsFeedAdapter.notifyDataSetChanged();
    }

    @Override
    public void toSuccessAvatarDataChange(ArrayList<Image> dataImage) {
        newsFeedAdapter.setData(dataImage);
        newsFeedAdapter.notifyDataSetChanged();
        getValueMyAvatar();
    }

    @Override
    public void toNotSuccessAvatarDataChange() {

    }

    @Override
    public void toChangeAvatar(UserInfo info, Image d) {
        myRef.child(d.getId() + "").child("urlAvatar").setValue(info.getUrlAvatarUser());
    }

    @Override
    public void toNotSuccessDataChange() {
        showNotify("To Data change not Success");
    }


    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }


    @Override
    public void onSuccess(Void aVoid) {
//        showNotify("onSuccess");
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        showNotify("onFailure " + e.toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_input_image:
//                CommonUtils.startIntentImage(getActivity(), REQUEST_CODE_IMAGE);
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_IMAGE);
                break;
            case R.id.iv_save_image:
                SendImageOrStatus();
                break;
            case R.id.tr_mail:
                toMyProfile();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
//            realpath = getRealPathFromURI(uri);
            try {
                InputStream inputStream = mContext.getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                binding.ivInputImage.setImageBitmap(bitmap);
                isChooseImage = true;
            } catch (FileNotFoundException e) {
                isChooseImage = false;
                e.printStackTrace();
            }
        } else {
            isChooseImage = false;
        }
    }

    private void toMyProfile() {
        getParent().showFragment(FragmentUserInfo.TAG, FragmentNewsFeed.TAG, null);
    }

    @Override
    public void showLoading() {
        if (firstNumber == 0) {
            binding.rlLogo.setVisibility(View.VISIBLE);
            binding.spinKitFirst.setVisibility(View.VISIBLE);
            firstNumber++;
        } else {
            binding.spinKit.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        binding.rlLogo.setVisibility(View.GONE);
        binding.spinKitFirst.setVisibility(View.GONE);
        binding.spinKit.setVisibility(View.GONE);
    }


    @Override
    public void showStatus(Boolean isShow) {
        if (isShow) {
            binding.lnContent.setVisibility(View.GONE);
        } else {
            binding.lnContent.setVisibility(View.VISIBLE);
        }
        PrefUtil.putBoolean(mContext, Constances.IS_SHOW_STATUS, !isShow);
    }


    @Override
    public void onHideLoading() {
        hideLoading();
        isChooseImage = false;
    }
}
