package com.example.myfirebase1.view.fragment;

import android.os.Handler;

import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragSplashBinding;
import com.example.myfirebase1.presenter.SplashPresenter;
import com.example.myfirebase1.view.event.OnSplashCallBack;

import is.arontibo.library.ProgressDownloadView;

public class FragmentSplash extends BaseFragment<SplashPresenter, FragSplashBinding> implements OnSplashCallBack {

    public static final String TAG = FragmentSplash.class.getName();

    @Override
    protected SplashPresenter getPresenter() {
        return new SplashPresenter(this);
    }

    @Override
    protected void initView() {

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                binding.elasticDownloadView.startIntro();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.elasticDownloadView.success();
            }
        }, 3 * ProgressDownloadView.ANIMATION_DURATION_BASE);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.elasticDownloadView.setProgress(65);
            }
        }, 2 * ProgressDownloadView.ANIMATION_DURATION_BASE);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
//                startActivity(intent);
//                finish();
                getParent().showFragment(FragmentLogin.TAG,null,null);

            }
        }, 5 * ProgressDownloadView.ANIMATION_DURATION_BASE);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_splash;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
