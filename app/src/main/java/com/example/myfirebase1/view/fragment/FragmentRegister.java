package com.example.myfirebase1.view.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragRegisterBinding;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.presenter.RegisterPresenter;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.utils.TextUtils;
import com.example.myfirebase1.view.event.OnRegisterCallBack;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class FragmentRegister extends BaseFragment<RegisterPresenter, FragRegisterBinding> implements OnRegisterCallBack, View.OnClickListener {

    public static final String TAG = FragmentRegister.class.getName();
    public static final String EXTRA_USER = "extra.user";
    public static final String EXTRA_PASS = "extra.pass";
    private FirebaseAuth mAuth;


    @Override
    protected RegisterPresenter getPresenter() {
        return new RegisterPresenter(this);
    }

    @Override
    protected void initView() {
        TextUtils.getInstance().setTextColor(binding.btnRegister);
        mAuth = FirebaseAuth.getInstance();
        binding.btnRegister.setOnClickListener(this);
        binding.ivBackRegister.setOnClickListener(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_register;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        putOfflineUser();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_register:
                showLoading();
                register();
                break;
            case R.id.iv_back_register:
                getParent().onBackPressed();
//                finish();
                break;
            default:
                break;

        }
    }

    private void register() {
        try {
            if (binding.edtEmail.getText().toString().isEmpty() || binding.edtPassword.getText().toString().isEmpty()){
                hideLoading();
                return;
            }
            final String email = binding.edtEmail.getText().toString();
            final String password = binding.edtPassword.getText().toString();
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
//                                showNotify("Authentication success");
                                showSnackbar(binding.rlContainerRegister,"Authentication success");
//                            FirebaseUser user = mAuth.getCurrentUser();

                                Bundle bundle = new Bundle();
                                bundle.putString(EXTRA_USER,email);
                                bundle.putString(EXTRA_PASS,password);
                                getParent().showFragment(FragmentLogin.TAG,null,bundle);
                                hideLoading();
                            }else {
                                showNotify("Authentication fail");
                                hideLoading();
                            }
                        }
                    });
        }catch (Exception ex){
            ex.printStackTrace();
            hideLoading();
        }
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }
}
