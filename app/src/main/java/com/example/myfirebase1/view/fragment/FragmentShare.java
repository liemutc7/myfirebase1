package com.example.myfirebase1.view.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragShareBinding;
import com.example.myfirebase1.presenter.SharePresenter;
import com.example.myfirebase1.view.event.OnShareCallBack;

import java.io.File;
import java.util.ArrayList;

public class FragmentShare extends BaseFragment<SharePresenter, FragShareBinding> implements OnShareCallBack, View.OnClickListener, BaseFragment.onBaseFragmentClick {

    public static final String TAG = FragmentShare.class.getName();
    private String imgFilePath;
    private String distance = "0";
    private String maxSpeed = "0";
    private String latitude = "0";
    private String longitude = "0";
//    private ArrayList<LatLngEntity> arrayPointImage = new ArrayList<>();
    private ArrayList<String> arrayPointLat;
    private ArrayList<String> arrayPointLong;

    @Override
    protected SharePresenter getPresenter() {
        return new SharePresenter(this);
    }

    @Override
    protected void initView() {
        setTitle("Share Location");

        arrayPointLat = new ArrayList<>();
        arrayPointLong = new ArrayList<>();

        if (getArguments() != null) {
            imgFilePath = getArguments().getString(FragmentLocation.PATH_IMAGE, "");
            distance = getArguments().getString(FragmentLocation.DISTANCE_ME, "");
            maxSpeed = getArguments().getString(FragmentLocation.SPEED_ME, "");
            latitude = getArguments().getString(FragmentLocation.LATITUDE_ME, "");
            longitude = getArguments().getString(FragmentLocation.LONGITUDE_ME, "");
//            arrayPointImage = (ArrayList<LatLngEntity>) getArguments().getSerializable(FragmentLocation.ARRAY_POINT_LAT_LNG);
            arrayPointLat = getArguments().getStringArrayList(FragmentLocation.ARRAY_POINT_LAT);
            arrayPointLong = getArguments().getStringArrayList(FragmentLocation.ARRAY_POINT_LONG);
        }

        File imgFile = new File(imgFilePath);


        if (imgFilePath != null) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFilePath);

            Glide.with(mContext)
                    .load(myBitmap)
                    .centerCrop()
                    .into(binding.ivShareImage);

            Log.d(TAG, "path s " + imgFilePath);

            binding.tvDistance.setText(distance);

            binding.tvSpeed.setText(maxSpeed);

            binding.tvLatitude.setText(latitude);

            binding.tvLongitude.setText(longitude);


//            binding.ivShareImage.setImageBitmap(myBitmap);

        }

        binding.btnShare.setOnClickListener(this);

        setListener(this);

        hideLoading();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_share;
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_share:
                showLoading();
                SendImageLocationStatus(true, binding.ivShareImage, binding.edtInput,distance,maxSpeed,latitude,longitude,arrayPointLat,arrayPointLong);
                break;
            default:
                break;
        }
    }

    @Override
    public void onHideLoading() {
        hideLoading();
        getParent().showFragment(FragmentNewsFeed.TAG, null, null);
    }
}
