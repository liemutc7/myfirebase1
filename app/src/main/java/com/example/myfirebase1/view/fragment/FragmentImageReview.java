package com.example.myfirebase1.view.fragment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.myfirebase1.R;
import com.example.myfirebase1.base.BaseFragment;
import com.example.myfirebase1.databinding.FragImageReviewBinding;
import com.example.myfirebase1.presenter.ImageReviewPresenter;
import com.example.myfirebase1.utils.CommonUtils;
import com.example.myfirebase1.utils.Constances;
import com.example.myfirebase1.view.event.OnImageReviewCallBack;

public class FragmentImageReview extends BaseFragment<ImageReviewPresenter, FragImageReviewBinding> implements OnImageReviewCallBack {

    public static final String TAG = FragmentImageReview.class.getName();

    private String imageUrl = "";

    @Override
    protected ImageReviewPresenter getPresenter() {
        return new ImageReviewPresenter(this);
    }

    @Override
    protected void initView() {
        if(getArguments()!=null){
            imageUrl = getArguments().getString(Constances.NEWS_FEED_IMAGE);
            Glide.with(mContext)
                    .load(imageUrl)
                    .into(binding.ivImage);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        CommonUtils.getInstance().changeStatusBar(getActivity(),R.color.ColorBlack);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
        CommonUtils.getInstance().changeStatusBar(getActivity(),R.color.ColorSplash);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_image_review;
    }

    @Override
    public void showLoading() {
        binding.spinKit.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.spinKit.setVisibility(View.GONE);
    }
}
