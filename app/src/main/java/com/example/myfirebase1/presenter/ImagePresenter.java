package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnImageCallBack;

public class ImagePresenter extends BasePresenter<OnImageCallBack>{
    public ImagePresenter(OnImageCallBack mCallBack) {
        super(mCallBack);
    }
}
