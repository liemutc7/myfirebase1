package com.example.myfirebase1.presenter;

import com.example.myfirebase1.view.event.OnSplashCallBack;

public class SplashPresenter extends BasePresenter<OnSplashCallBack>{
    public SplashPresenter(OnSplashCallBack mCallBack) {
        super(mCallBack);
    }
}
