package com.example.myfirebase1.presenter;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.myfirebase1.model.SortbyTotalRun;
import com.example.myfirebase1.model.UserInfo;
import com.example.myfirebase1.view.event.OnRankDistanceCallBack;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Collections;

public class RankDistancePresenter extends BasePresenter<OnRankDistanceCallBack> {
    public RankDistancePresenter(OnRankDistanceCallBack mCallBack) {
        super(mCallBack);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void dataChangeRank(ArrayList<UserInfo> dataInfo, DataSnapshot dataSnapshot) {
        mCallBack.showLoading();
        try {
            dataInfo.clear();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                UserInfo info = data.getValue(UserInfo.class);
                dataInfo.add(info);
            }


            Collections.sort(dataInfo, new SortbyTotalRun());

                mCallBack.toSuccessRankDataChange(dataInfo);
                mCallBack.hideLoading();
            }catch(Exception ex){
                ex.printStackTrace();
                mCallBack.toNotSuccessRankDataChange();
                mCallBack.hideLoading();
            }
        }
    }
